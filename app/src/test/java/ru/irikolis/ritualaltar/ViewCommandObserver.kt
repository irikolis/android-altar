package ru.irikolis.ritualaltar

import androidx.lifecycle.Observer

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class ViewCommandObserver<T>: Observer<T> {
    override fun onChanged(t: T?) {
    }
}