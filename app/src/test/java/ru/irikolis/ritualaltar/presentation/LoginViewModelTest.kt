package ru.irikolis.ritualaltar.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import ru.irikolis.ritualaltar.ViewCommandObserver
import ru.irikolis.ritualaltar.data.preference.AppPreference
import ru.irikolis.ritualaltar.domain.interactors.AuthInteractor
import ru.irikolis.ritualaltar.presentation.authorization.login.LoginViewModel
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */

private const val EMPTY_ADDRESS = ""
private const val ADDRESS = "255.255.255.255:8000"
private const val EMPTY_USERNAME = ""
private const val CORRECT_USERNAME = "CorrectUsername"
private const val WRONG_USERNAME = "WrongUsername"
private const val EMPTY_PASSWORD = ""
private const val CORRECT_PASSWORD = "CorrectPassword"
private const val WRONG_PASSWORD = "WrongPassword"

@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var appPreference: AppPreference

    @Mock
    lateinit var authInteractor: AuthInteractor

    @Mock
    lateinit var schedulersProvider: SchedulersProvider

    private lateinit var viewModel: LoginViewModel

    @Mock
    lateinit var isLoadingObserverView: ViewCommandObserver<Boolean>

    @Mock
    lateinit var addressValidViewCommandObserver: ViewCommandObserver<Boolean>

    @Mock
    lateinit var usernameValidViewCommandObserver: ViewCommandObserver<Boolean>

    @Mock
    lateinit var passwordValidViewCommandObserver: ViewCommandObserver<Boolean>

    @Mock
    lateinit var loginSuccessViewCommandObserver: ViewCommandObserver<Void>

    @Mock
    lateinit var loginErrorViewCommandObserver: ViewCommandObserver<Void>

    @Before
    fun setUp() {
        `when`(appPreference.baseUrl).thenReturn(ADDRESS)
        `when`(schedulersProvider.ui()).thenReturn(Schedulers.trampoline())
        `when`(authInteractor.performAuthLogin(CORRECT_USERNAME, CORRECT_PASSWORD)).thenReturn(Single.just(true))
        `when`(authInteractor.performAuthLogin(WRONG_USERNAME, WRONG_PASSWORD)).thenReturn(Single.error(Throwable()))

        viewModel = LoginViewModel(appPreference, authInteractor, schedulersProvider)
        viewModel.isLoading.observeForever(isLoadingObserverView)
        viewModel.addressValidCommand.observeForever(addressValidViewCommandObserver)
        viewModel.usernameValidCommand.observeForever(usernameValidViewCommandObserver)
        viewModel.passwordValidCommand.observeForever(passwordValidViewCommandObserver)
        viewModel.loginSuccessCommand.observeForever(loginSuccessViewCommandObserver)
        viewModel.loginErrorCommand.observeForever(loginErrorViewCommandObserver)
    }

    @Test
    fun testEmptyAddress() {
        viewModel.address.value = EMPTY_ADDRESS
        viewModel.username.value = CORRECT_USERNAME
        viewModel.password.value = CORRECT_PASSWORD

        viewModel.onAuthClick()

        verify(addressValidViewCommandObserver).onChanged(false)
        verify(usernameValidViewCommandObserver).onChanged(true)
        verify(passwordValidViewCommandObserver).onChanged(true)

        verify(loginSuccessViewCommandObserver, never()).onChanged(null)
        verify(loginErrorViewCommandObserver, never()).onChanged(null)
    }

    @Test
    fun testEmptyUsername() {
        viewModel.address.value = ADDRESS
        viewModel.username.value = EMPTY_USERNAME
        viewModel.password.value = CORRECT_PASSWORD

        viewModel.onAuthClick()

        verify(addressValidViewCommandObserver).onChanged(true)
        verify(usernameValidViewCommandObserver).onChanged(false)
        verify(passwordValidViewCommandObserver).onChanged(true)

        verify(loginSuccessViewCommandObserver, never()).onChanged(null)
        verify(loginErrorViewCommandObserver, never()).onChanged(null)
    }

    @Test
    fun testEmptyPassword() {
        viewModel.address.value = ADDRESS
        viewModel.username.value = CORRECT_USERNAME
        viewModel.password.value = EMPTY_PASSWORD

        viewModel.onAuthClick()

        verify(addressValidViewCommandObserver).onChanged(true)
        verify(usernameValidViewCommandObserver).onChanged(true)
        verify(passwordValidViewCommandObserver).onChanged(false)

        verify(loginSuccessViewCommandObserver, never()).onChanged(null)
        verify(loginErrorViewCommandObserver, never()).onChanged(null)
    }

    @Test
    fun testLoginSuccess() {
        viewModel.username.value = CORRECT_USERNAME
        viewModel.password.value = CORRECT_PASSWORD

        verify(isLoadingObserverView).onChanged(false)
        clearInvocations(isLoadingObserverView)

        viewModel.onAuthClick()

        verify(addressValidViewCommandObserver).onChanged(true)
        verify(usernameValidViewCommandObserver).onChanged(true)
        verify(passwordValidViewCommandObserver).onChanged(true)

        verify(isLoadingObserverView).onChanged(true)
        verify(isLoadingObserverView).onChanged(false)

        verify(loginSuccessViewCommandObserver).onChanged(null)
        verify(loginErrorViewCommandObserver, never()).onChanged(null)
    }

    @Test
    fun testLoginError() {
        viewModel.username.value = WRONG_USERNAME
        viewModel.password.value = WRONG_PASSWORD

        verify(isLoadingObserverView).onChanged(false)
        clearInvocations(isLoadingObserverView)

        viewModel.onAuthClick()

        verify(addressValidViewCommandObserver).onChanged(true)
        verify(usernameValidViewCommandObserver).onChanged(true)
        verify(passwordValidViewCommandObserver).onChanged(true)

        verify(isLoadingObserverView).onChanged(true)
        verify(isLoadingObserverView).onChanged(false)

        verify(loginErrorViewCommandObserver).onChanged(null)
        verify(loginSuccessViewCommandObserver, never()).onChanged(null)
    }
}