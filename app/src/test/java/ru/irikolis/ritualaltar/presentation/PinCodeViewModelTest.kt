package ru.irikolis.ritualaltar.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import ru.irikolis.ritualaltar.ViewCommandObserver
import ru.irikolis.ritualaltar.data.preference.AppPreference
import ru.irikolis.ritualaltar.domain.interactors.AuthInteractor
import ru.irikolis.ritualaltar.presentation.authorization.pincode.PinCodeViewModel
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */

private const val EMPTY_ADDRESS = ""
private const val ADDRESS = "255.255.255.255:8000"
private const val EMPTY_PIN_CODE = ""
private const val CORRECT_PIN_CODE = "CorrectPinCode"
private const val WRONG_PIN_CODE = "WrongPinCode"

@RunWith(MockitoJUnitRunner::class)
class PinCodeViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var appPreference: AppPreference

    @Mock
    lateinit var authInteractor: AuthInteractor

    @Mock
    lateinit var schedulersProvider: SchedulersProvider

    private lateinit var viewModel: PinCodeViewModel

    @Mock
    lateinit var isLoadingObserverView: ViewCommandObserver<Boolean>

    @Mock
    lateinit var addressValidViewCommandObserver: ViewCommandObserver<Boolean>

    @Mock
    lateinit var pinCodeValidViewCommandObserver: ViewCommandObserver<Boolean>

    @Mock
    lateinit var pinCodeSuccessViewCommandObserver: ViewCommandObserver<Void>

    @Mock
    lateinit var pinCodeErrorViewCommandObserver: ViewCommandObserver<Void>

    @Before
    fun setUp() {
        `when`(appPreference.baseUrl).thenReturn(ADDRESS)
        `when`(schedulersProvider.ui()).thenReturn(Schedulers.trampoline())
        `when`(authInteractor.performAuthPinCode(CORRECT_PIN_CODE)).thenReturn(Single.just(true))
        `when`(authInteractor.performAuthPinCode(WRONG_PIN_CODE)).thenReturn(Single.error(Throwable()))

        viewModel = PinCodeViewModel(appPreference, authInteractor, schedulersProvider)

        viewModel.isLoading.observeForever(isLoadingObserverView)
        viewModel.addressValidCommand.observeForever(addressValidViewCommandObserver)
        viewModel.pinCodeValidCommand.observeForever(pinCodeValidViewCommandObserver)
        viewModel.pinCodeSuccessCommand.observeForever(pinCodeSuccessViewCommandObserver)
        viewModel.pinCodeErrorCommand.observeForever(pinCodeErrorViewCommandObserver)
    }

    @Test
    fun testEmptyAddress() {
        viewModel.address.value = EMPTY_ADDRESS
        viewModel.pinCode.value = CORRECT_PIN_CODE

        viewModel.onSignInClick()

        verify(addressValidViewCommandObserver).onChanged(false)
        verify(pinCodeValidViewCommandObserver).onChanged(true)

        verify(pinCodeSuccessViewCommandObserver, never()).onChanged(null)
        verify(pinCodeErrorViewCommandObserver, never()).onChanged(null)
    }

    @Test
    fun testEmptyPinCode() {
        viewModel.address.value = ADDRESS
        viewModel.pinCode.value = EMPTY_PIN_CODE

        viewModel.onSignInClick()

        verify(pinCodeValidViewCommandObserver).onChanged(false)
        verify(addressValidViewCommandObserver).onChanged(true)

        verify(pinCodeSuccessViewCommandObserver, never()).onChanged(null)
        verify(pinCodeErrorViewCommandObserver, never()).onChanged(null)
    }

    @Test
    fun testPinCodeSuccess() {
        viewModel.pinCode.value = CORRECT_PIN_CODE

        verify(isLoadingObserverView).onChanged(false)
        clearInvocations(isLoadingObserverView)

        viewModel.onSignInClick()

        verify(pinCodeValidViewCommandObserver).onChanged(true)
        verify(addressValidViewCommandObserver).onChanged(true)

        verify(isLoadingObserverView).onChanged(true)
        verify(isLoadingObserverView).onChanged(false)

        verify(pinCodeSuccessViewCommandObserver).onChanged(null)
        verify(pinCodeErrorViewCommandObserver, never()).onChanged(null)
    }

    @Test
    fun testPinCodeError() {
        viewModel.pinCode.value = WRONG_PIN_CODE

        verify(isLoadingObserverView).onChanged(false)
        clearInvocations(isLoadingObserverView)

        viewModel.onSignInClick()

        verify(pinCodeValidViewCommandObserver).onChanged(true)
        verify(addressValidViewCommandObserver).onChanged(true)

        verify(isLoadingObserverView).onChanged(true)
        verify(isLoadingObserverView).onChanged(false)

        verify(pinCodeErrorViewCommandObserver).onChanged(null)
        verify(pinCodeSuccessViewCommandObserver, never()).onChanged(null)
    }
}