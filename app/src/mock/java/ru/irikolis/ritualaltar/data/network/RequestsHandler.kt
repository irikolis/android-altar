package ru.irikolis.ritualaltar.data.network

import okhttp3.Request
import okhttp3.Response
import ru.irikolis.ritualaltar.RitualAltarApplication
import java.io.IOException
import java.util.*

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class RequestsHandler {
    private val responses = HashMap<String, String>()

    init {
        responses[ApiService.REQUEST_AUTH_TOKEN] = "auth.json"
        responses[ApiService.REQUEST_GODS] = "gods.json"
        responses[ApiService.REQUEST_BEACONS] = "beacons.json"
        responses[ApiService.REQUEST_RITUALS] = "rituals.json"
        responses["${ApiService.REQUEST_RITUALS}1"] = "ritual_1.json"
        responses[ApiService.REQUEST_PRE_RITUAL] = "preritual.json"
        responses[ApiService.REQUEST_ACTIVATE] = "activate.json"
    }

    fun shouldIntercept(path: String): Boolean = responses.containsKey(path)

    fun proceed(request: Request, path: String): Response {
        if (responses.containsKey(path)) {
            val assetPath = responses[path]
            if (assetPath != null) {
                return createResponseFromAssets(request, assetPath)
            }
        }

        return OkHttpResponse.error(request, 500, "Incorrectly intercepted request")
    }

    private fun createResponseFromAssets(request: Request, assetPath: String): Response {
        return try {
            RitualAltarApplication.application.applicationContext?.assets?.open(assetPath)?.use { OkHttpResponse.success(request, it) }!!
        } catch (e: IOException) {
            OkHttpResponse.error(request, 500, e.message!!)
        }
    }
}