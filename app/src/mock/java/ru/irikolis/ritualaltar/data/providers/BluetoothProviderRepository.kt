package ru.irikolis.ritualaltar.data.providers

import android.content.Context
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Suppress("unused")
class BluetoothProviderRepository(
    private val context: Context
) {

    private val macs = arrayOf(
        "00:11:22:AA:BB:CC",
        "02:11:22:AA:BB:CC",
        "03:11:22:AA:BB:CC",
        "04:11:22:AA:BB:CC",
        "05:11:22:AA:BB:CC",
        "06:11:22:AA:BB:CC",
        "07:11:22:AA:BB:CC",
        "08:11:22:AA:BB:CC",
        "09:11:22:AA:BB:CC",
        "11:11:22:AA:BB:CC",
        "12:11:22:AA:BB:CC",
        "13:11:22:AA:BB:CC",
        "14:11:22:AA:BB:CC",
        "15:11:22:AA:BB:CC",
        "16:11:22:AA:BB:CC",
        "f1:11:22:AA:BB:CC",
        "f2:11:22:AA:BB:CC"
    )

    fun startBluetoothListener(): Observable<String> {
        return Observable.interval(1, TimeUnit.SECONDS)
            .take(macs.size.toLong())
            .map { macs[it.toInt()] }
    }
}