package ru.irikolis.ritualaltar.data.network

import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.asResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody
import okio.Buffer
import java.io.IOException
import java.io.InputStream

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
object OkHttpResponse {
    private val APPLICATION_JSON = "application/json".toMediaTypeOrNull()
    private val EMPTY_BODY = ByteArray(0)

    @Throws(IOException::class)
    fun success(request: Request, stream: InputStream): Response {
        val buffer = Buffer().readFrom(stream)
        return Response.Builder()
            .request(request)
            .protocol(Protocol.HTTP_1_1)
            .code(200)
            .message("OK")
            .body(buffer.asResponseBody(APPLICATION_JSON, buffer.size))
            .build()
    }

    fun error(request: Request, code: Int, message: String): Response {
        return Response.Builder()
            .request(request)
            .protocol(Protocol.HTTP_1_1)
            .code(code)
            .message(message)
            .body(EMPTY_BODY.toResponseBody(APPLICATION_JSON))
            .build()
    }
}