package ru.irikolis.ritualaltar.data.network

import okhttp3.OkHttpClient
import ru.irikolis.ritualaltar.AppConstants
import ru.irikolis.ritualaltar.data.preference.AppPreference
import java.util.concurrent.TimeUnit

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
object OkHttpProvider {
    fun buildClient(preference: AppPreference): OkHttpClient = OkHttpClient.Builder()
            .connectTimeout(AppConstants.NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
            .readTimeout(AppConstants.NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
            .writeTimeout(AppConstants.NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
            .addInterceptor(ApiKeyInterceptor(preference))
            .addInterceptor(LoggingInterceptor())
            .addInterceptor(MockingInterceptor())
            .build()
}