package ru.irikolis.ritualaltar.data.network

import android.os.SystemClock
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import java.security.SecureRandom

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class MockingInterceptor : Interceptor {
    private var handlers = RequestsHandler()
    private var random = SecureRandom()

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val path = request.url.encodedPath
        if (handlers.shouldIntercept(path)) {
            val response = handlers.proceed(request, path)
            val stubDelay = 50 + random.nextInt(2500)
            SystemClock.sleep(stubDelay.toLong())
            return response
        }

        return chain.proceed(request)
    }
}