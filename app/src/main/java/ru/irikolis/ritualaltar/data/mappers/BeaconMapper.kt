package ru.irikolis.ritualaltar.data.mappers

import ru.irikolis.ritualaltar.data.database.beacon.BeaconDb
import ru.irikolis.ritualaltar.data.network.beacons.BeaconResponse
import ru.irikolis.ritualaltar.domain.models.Beacon
import ru.irikolis.ritualaltar.domain.models.BeaconType

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
object BeaconMapper {
    fun networkToEntity(response: BeaconResponse) =
        Beacon(
            response.id,
            response.mac!!,
            response.name!!,
            response.description!!,
            response.force,
            BeaconType.valueOf(response.type!!)
        )

    fun dbToEntity(beaconDb: BeaconDb) =
        Beacon(
            beaconDb.id,
            beaconDb.mac,
            beaconDb.name,
            beaconDb.description,
            beaconDb.force,
            BeaconType.valueOf(beaconDb.type)
        )

    fun entityToDb(beacon: Beacon) =
        BeaconDb(
            beacon.id,
            beacon.mac,
            beacon.name,
            beacon.description,
            beacon.force,
            beacon.type.name
        )
}
