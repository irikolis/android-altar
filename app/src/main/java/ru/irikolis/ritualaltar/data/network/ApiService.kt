package ru.irikolis.ritualaltar.data.network

import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import ru.irikolis.ritualaltar.data.network.activate.ActivateRitualRequest
import ru.irikolis.ritualaltar.data.network.activate.ActivateRitualResponse
import ru.irikolis.ritualaltar.data.network.auth.AuthLoginRequest
import ru.irikolis.ritualaltar.data.network.auth.AuthLoginResponse
import ru.irikolis.ritualaltar.data.network.auth.AuthPinCodeRequest
import ru.irikolis.ritualaltar.data.network.auth.AuthPinCodeResponse
import ru.irikolis.ritualaltar.data.network.beacons.BeaconResponse
import ru.irikolis.ritualaltar.data.network.gods.GodsResponse
import ru.irikolis.ritualaltar.data.network.preritual.PreRitualRequest
import ru.irikolis.ritualaltar.data.network.preritual.PreRitualResponse
import ru.irikolis.ritualaltar.data.network.rituals.RitualsResponse

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
interface ApiService {

    companion object {
        const val REQUEST_AUTH_TOKEN = "/api/auth-token/"
        const val REQUEST_GODS = "/api/gods/"
        const val REQUEST_BEACONS = "/api/beacons/"
        const val REQUEST_RITUALS = "/api/rituals/"
        const val REQUEST_PRE_RITUAL = "/api/preritual/"
        const val REQUEST_ACTIVATE = "/api/activate/"
    }

    @POST(REQUEST_AUTH_TOKEN)
    fun performAuthLogin(@Body authLoginRequest: AuthLoginRequest): Single<AuthLoginResponse>

    @POST(REQUEST_AUTH_TOKEN)
    fun performAuthPinCode(@Body authPinCodeRequest: AuthPinCodeRequest): Single<AuthPinCodeResponse>

    @GET(REQUEST_GODS)
    fun getGods(): Single<GodsResponse>

    @GET(REQUEST_BEACONS)
    fun getBeacons(): Single< List<BeaconResponse> >

    @GET(REQUEST_RITUALS)
    fun getRituals(): Single<RitualsResponse>

    @POST(REQUEST_PRE_RITUAL)
    fun performPreRitual(@Body preRitualRequest: PreRitualRequest): Single<PreRitualResponse>

    @POST(REQUEST_ACTIVATE)
    fun performActivateRitual(@Body activateRitualRequest: ActivateRitualRequest): Single<ActivateRitualResponse>
}