package ru.irikolis.ritualaltar.data.database.collected

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "collected_beacons")
data class CollectedBeaconDb(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,

    @ColumnInfo(name = "mac")
    val mac: String,
)