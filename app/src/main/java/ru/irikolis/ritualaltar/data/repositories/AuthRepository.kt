package ru.irikolis.ritualaltar.data.repositories

import io.reactivex.Single
import ru.irikolis.ritualaltar.data.database.AppDatabase
import ru.irikolis.ritualaltar.data.network.ApiService
import ru.irikolis.ritualaltar.data.network.auth.AuthLoginRequest
import ru.irikolis.ritualaltar.data.network.auth.AuthPinCodeRequest
import ru.irikolis.ritualaltar.data.preference.AppPreference

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class AuthRepository(
    private val apiService: ApiService,
    private val appPreference: AppPreference,
    private val appDatabase: AppDatabase
) {

    fun performAuthLoginFromApi(username: String, password: String): Single<Boolean> {
        return apiService.performAuthLogin(AuthLoginRequest(username, password))
            .doOnSuccess { response ->
                appPreference.token = response.token
                if (response.lastUpdate != appPreference.lastUpdate) {
                    appPreference.lastUpdate = response.lastUpdate
                    appDatabase.clearAllTables()
                }
            }
            .map { it.token }
            .map { it.isNotEmpty() }
    }

    fun performAuthPinCodeFromApi(pinCode: String): Single<Boolean> {
        return apiService.performAuthPinCode(AuthPinCodeRequest(pinCode))
            .doOnSuccess { response ->
                appPreference.token = response.token
                if (response.lastUpdate != appPreference.lastUpdate) {
                    appPreference.lastUpdate = response.lastUpdate
                    appDatabase.clearAllTables()
                }
            }
            .map { it.token }
            .map { it.isNotEmpty() }
    }
}