package ru.irikolis.ritualaltar.data.database.beacon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "beacons")
data class BeaconDb(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,

    @ColumnInfo(name = "mac")
    val mac: String,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "description")
    val description: String,

    @ColumnInfo(name = "force")
    val force: Int,

    @ColumnInfo(name = "type")
    val type: String
)