package ru.irikolis.ritualaltar.data.network.gods

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class GodResponse(
    @Expose
    @SerializedName("god_id")
    val godId: Int = 0,

    @Expose
    @SerializedName("god_name")
    val godName: String? = null
)