package ru.irikolis.ritualaltar.data.database.collected

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
interface CollectedBeaconDao {
    @Query("SELECT * FROM collected_beacons")
    fun getAll(): List<CollectedBeaconDb>

    @Insert
    fun insert(collectedBeaconDb: CollectedBeaconDb): Long
}