package ru.irikolis.ritualaltar.data.database.ritual

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
interface RitualDao {
    @Query("SELECT * FROM rituals")
    fun getAll(): List<RitualDb>

    @Query("SELECT * FROM rituals WHERE id=:ritualId")
    fun getById(ritualId: Int): List<RitualDb>

    @Insert
    fun insert(ritualDb: RitualDb): Long
}