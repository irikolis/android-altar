package ru.irikolis.ritualaltar.data.network.beacons

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class BeaconResponse(
    @Expose
    @SerializedName("id")
    val id: Int = 0,

    @Expose
    @SerializedName("mac")
    val mac: String? = null,

    @Expose
    @SerializedName("name")
    val name: String? = null,

    @Expose
    @SerializedName("description")
    val description: String? = null,

    @Expose
    @SerializedName("force")
    val force: Int = 0,

    @Expose
    @SerializedName("type")
    val type: String? = null
)