package ru.irikolis.ritualaltar.data.database.ritual

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "rituals")
data class RitualDb(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val ritualId: Int = 0,

    @ColumnInfo(name = "god_id")
    val godId: Int,

    @ColumnInfo(name = "ritual_name")
    val ritualName: String,

    @ColumnInfo(name = "priest_cnt")
    val priestCnt: Int,

    @ColumnInfo(name = "senior_priest_cnt")
    val seniorPriestCnt: Int,

    @ColumnInfo(name = "resources")
    val resources: String,

    @ColumnInfo(name = "effect")
    val effect: String
)