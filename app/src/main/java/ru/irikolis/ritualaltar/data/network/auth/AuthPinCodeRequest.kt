package ru.irikolis.ritualaltar.data.network.auth

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class AuthPinCodeRequest(
    @Expose
    @SerializedName("pincode")
    val pinCode: String,
)