package ru.irikolis.ritualaltar.data.network.auth

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class AuthLoginRequest(
    @Expose
    @SerializedName("username")
    val username: String,

    @Expose
    @SerializedName("password")
    val password: String
)