package ru.irikolis.ritualaltar.data.database.beacon

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
interface BeaconDao {
    @Query("SELECT * FROM beacons")
    fun getAll(): List<BeaconDb>

    @Insert
    fun insert(beaconDb: BeaconDb): Long
}