package ru.irikolis.ritualaltar.data.mappers

import ru.irikolis.ritualaltar.data.database.god.GodDb
import ru.irikolis.ritualaltar.data.network.gods.GodResponse
import ru.irikolis.ritualaltar.domain.models.God

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
object GodMapper {
    fun networkToEntity(response: GodResponse) =
        God(
            response.godId,
            response.godName!!
        )

    fun dbToEntity(godDb: GodDb) =
        God(
            godDb.id,
            godDb.name
        )

    fun entityToDb(god: God) =
        GodDb(
            god.godId,
            god.name
        )
}
