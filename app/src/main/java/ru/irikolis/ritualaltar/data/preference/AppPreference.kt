package ru.irikolis.ritualaltar.data.preference

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import ru.irikolis.ritualaltar.AppConstants

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class AppPreference(context: Context, prefName: String) {
    companion object {
        private const val PREF_KEY_BASE_URL = "base_url"
        private const val PREF_KEY_TOKEN = "token"
        private const val PREF_KEY_LAST_UPDATE = "last_update"
        private const val PREF_KEY_GOD_ID = "god_id"
        private const val PREF_KEY_RITUAL_ID = "ritual_id"
    }

    private var sharedPreferences: SharedPreferences = context.getSharedPreferences(prefName, Context.MODE_PRIVATE)

    var baseUrl: String
        get() = sharedPreferences.getString(PREF_KEY_BASE_URL, AppConstants.BASE_URL)!!
        set(value) { sharedPreferences.edit { putString(PREF_KEY_BASE_URL, value) } }

    var token: String
        get() = sharedPreferences.getString(PREF_KEY_TOKEN, "")!!
        set(value) { sharedPreferences.edit { putString(PREF_KEY_TOKEN, value) } }

    var lastUpdate: String
        get() = sharedPreferences.getString(PREF_KEY_LAST_UPDATE, "")!!
        set(value) { sharedPreferences.edit { putString(PREF_KEY_LAST_UPDATE, value) } }

    var godId: Int
        get() = sharedPreferences.getInt(PREF_KEY_GOD_ID, 0)
        set(value) { sharedPreferences.edit { putInt(PREF_KEY_GOD_ID, value) } }

    var ritualId: Int
        get() = sharedPreferences.getInt(PREF_KEY_RITUAL_ID, 0)
        set(value) { sharedPreferences.edit { putInt(PREF_KEY_RITUAL_ID, value) } }
}