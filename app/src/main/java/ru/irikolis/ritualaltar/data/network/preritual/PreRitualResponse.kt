package ru.irikolis.ritualaltar.data.network.preritual

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class PreRitualResponse(
    @Expose
    @SerializedName("result")
    val result: String? = null
)