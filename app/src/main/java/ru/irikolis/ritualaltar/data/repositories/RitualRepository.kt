package ru.irikolis.ritualaltar.data.repositories

import io.reactivex.Single
import ru.irikolis.ritualaltar.data.database.AppDatabase
import ru.irikolis.ritualaltar.data.mappers.RitualMapper
import ru.irikolis.ritualaltar.data.network.ApiService
import ru.irikolis.ritualaltar.data.network.activate.ActivateRitualRequest
import ru.irikolis.ritualaltar.data.network.preritual.PreRitualRequest
import ru.irikolis.ritualaltar.domain.models.ActivateRitual
import ru.irikolis.ritualaltar.domain.models.PreRitual
import ru.irikolis.ritualaltar.domain.models.Ritual

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class RitualRepository(
    private val apiService: ApiService,
    private val appDatabase: AppDatabase
) {

    fun getRitualsFromApi(): Single<List<Ritual>> {
        return apiService.getRituals()
            .map { response -> response.rituals }
            .flattenAsObservable { items -> items }
            .map(RitualMapper::networkToEntity)
            .toList()
    }


    fun getRitualsFromDb(): Single<List<Ritual>> {
        return Single.fromCallable { appDatabase.getRitualDao().getAll() }
            .flattenAsObservable { items -> items }
            .map(RitualMapper::dbToEntity)
            .toList()
    }

    fun resetRitualsDb(rituals: List<Ritual>): Single<List<Long>> {
        return Single.fromCallable { appDatabase.clearTable("rituals") }
            .map { rituals }
            .flattenAsObservable { items -> items }
            .map(RitualMapper::entityToDb)
            .map { ritual -> appDatabase.getRitualDao().insert(ritual) }
            .toList()
    }

    fun performPreRitualFromApi(ritualId: Int, beaconIds: IntArray, quality: Int, dice: Int): Single<PreRitual> {
        return apiService.performPreRitual(PreRitualRequest(ritualId, beaconIds, quality, dice))
            .map(RitualMapper::networkToEntity)
    }

    fun performActivateRitualFromApi(ritualId: Int, beaconIds: IntArray, quality: Int, dice: Int): Single<ActivateRitual> {
        return apiService.performActivateRitual(ActivateRitualRequest(ritualId, beaconIds, quality, dice))
            .map(RitualMapper::networkToEntity)
    }

    fun getRitualResultFromDb(): Single<String> {
        return Single.fromCallable { appDatabase.getResultHistoryDao().getLastRow() }
            .flattenAsObservable { items -> items }
            .firstOrError()
            .map { row -> row.result }
    }

    fun putResultToDb(ritualResult: String): Single<Long> {
        return Single.fromCallable { ritualResult }
            .map(RitualMapper::entityToDb)
            .map { result -> appDatabase.getResultHistoryDao().insert(result) }
    }
}