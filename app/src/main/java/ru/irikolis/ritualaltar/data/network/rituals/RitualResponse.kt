package ru.irikolis.ritualaltar.data.network.rituals

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class RitualResponse(
    @Expose
    @SerializedName("ritual_id")
    val ritualId: Int = 0,

    @Expose
    @SerializedName("god_id")
    val godId: Int = 0,

    @Expose
    @SerializedName("ritual_name")
    val ritualName: String? = null,

    @Expose
    @SerializedName("priest_cnt")
    val priestCnt: Int = 0,

    @Expose
    @SerializedName("senior_priest_cnt")
    val seniorPriestCnt: Int = 0,

    @Expose
    @SerializedName("resources")
    val resources: String? = null,

    @Expose
    @SerializedName("effect")
    val effect: String? = null
)