package ru.irikolis.ritualaltar.data.network.preritual

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class PreRitualRequest(
    @Expose
    @SerializedName("ritual_id")
    val ritualId: Int,

    @Expose
    @SerializedName("beacons_ids")
    val beaconIds: IntArray,

    @Expose
    @SerializedName("quality")
    val quality: Int,

    @Expose
    @SerializedName("dice")
    val dice: Int
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PreRitualRequest

        if (!beaconIds.contentEquals(other.beaconIds)) return false

        return true
    }

    override fun hashCode(): Int {
        return beaconIds.contentHashCode()
    }
}