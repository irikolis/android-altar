package ru.irikolis.ritualaltar.data.mappers

import ru.irikolis.ritualaltar.data.database.collected.CollectedBeaconDb
import ru.irikolis.ritualaltar.domain.models.CollectedBeacon

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
object CollectedBeaconMapper {
    fun dbToEntity(collectedBeaconDb: CollectedBeaconDb) =
        CollectedBeacon(
            collectedBeaconDb.id,
            collectedBeaconDb.mac
        )

    fun entityToDb(collectedBeacon: CollectedBeacon) =
        CollectedBeaconDb(
            collectedBeacon.id,
            collectedBeacon.mac
        )
}
