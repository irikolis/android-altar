package ru.irikolis.ritualaltar.data.network.auth

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class AuthPinCodeResponse(
    @Expose
    @SerializedName("token")
    val token: String = "",

    @Expose
    @SerializedName("user_id")
    val userId: Int = 0,

    @Expose
    @SerializedName("last_update")
    val lastUpdate: String = ""
)