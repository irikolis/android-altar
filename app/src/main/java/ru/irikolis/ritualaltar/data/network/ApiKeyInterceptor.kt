package ru.irikolis.ritualaltar.data.network

import okhttp3.Interceptor
import okhttp3.Response
import ru.irikolis.ritualaltar.data.preference.AppPreference
import java.io.IOException

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class ApiKeyInterceptor(private val appPreference: AppPreference) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()

        val baseUrl = appPreference.baseUrl
        if (baseUrl.isNotEmpty()) {
            val url = StringBuilder()
            url.append(String.format("http://%s", baseUrl))
            url.append(chain.request().url.encodedPath)
            if (chain.request().url.encodedQuery != null)
                url.append(String.format("?%s", chain.request().url.encodedQuery!!))

            request.url(url.toString())
        } else
            request.url(chain.request().url)

        val token = appPreference.token
        if (token.isNotEmpty())
            request.addHeader("Authorization", String.format("Token %s", token))

        return chain.proceed(request.build())
    }
}