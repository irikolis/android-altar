package ru.irikolis.ritualaltar.data.database.god

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
interface GodDao {
    @Query("SELECT * FROM gods")
    fun getAll(): List<GodDb>

    @Insert
    fun insert(godDb: GodDb): Long
}