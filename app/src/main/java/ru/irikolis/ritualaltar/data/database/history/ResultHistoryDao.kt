package ru.irikolis.ritualaltar.data.database.history

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Dao
interface ResultHistoryDao {
    @Query("SELECT * FROM result_history")
    fun getAll(): List<ResultHistoryDb>

    @Query("SELECT * FROM result_history ORDER BY id DESC LIMIT 1")
    fun getLastRow(): List<ResultHistoryDb>

    @Insert
    fun insert(resultHistoryDb: ResultHistoryDb): Long
}