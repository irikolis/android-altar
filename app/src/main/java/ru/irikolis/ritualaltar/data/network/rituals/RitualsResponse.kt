package ru.irikolis.ritualaltar.data.network.rituals

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class RitualsResponse(
    @Expose
    @SerializedName("ritual_count")
    val ritualCount: Int = 0,

    @Expose
    @SerializedName("rituals")
    val rituals: List<RitualResponse> = ArrayList()
)