package ru.irikolis.ritualaltar.data.network.activate

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class ActivateRitualResponse(
    @Expose
    @SerializedName("result")
    val result: String? = null
)