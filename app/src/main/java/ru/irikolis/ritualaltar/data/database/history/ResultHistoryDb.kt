package ru.irikolis.ritualaltar.data.database.history

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "result_history")
data class ResultHistoryDb(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,

    @ColumnInfo(name = "ritual_result")
    val result: String
)