package ru.irikolis.ritualaltar.data.repositories

import io.reactivex.Single
import ru.irikolis.ritualaltar.data.database.AppDatabase
import ru.irikolis.ritualaltar.data.mappers.GodMapper
import ru.irikolis.ritualaltar.data.network.ApiService
import ru.irikolis.ritualaltar.domain.models.God

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class GodRepository (
    private val apiService: ApiService,
    private val appDatabase: AppDatabase
) {

    fun getGodsFromApi(): Single<List<God>> {
        return apiService.getGods()
            .map { response -> response.gods }
            .flattenAsObservable { items -> items }
            .map(GodMapper::networkToEntity)
            .toList()
    }

    fun getGodsFromDb(): Single<List<God>> {
        return Single.fromCallable { appDatabase.getGodDao().getAll() }
            .flattenAsObservable { items -> items }
            .map(GodMapper::dbToEntity)
            .toList()
    }

    fun resetGodsDb(gods: List<God>): Single<List<Long>> {
        return Single.fromCallable { appDatabase.clearTable("gods") }
            .map { gods }
            .flattenAsObservable { items -> items }
            .map(GodMapper::entityToDb)
            .map { god -> appDatabase.getGodDao().insert(god) }
            .toList()
    }
}