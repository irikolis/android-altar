package ru.irikolis.ritualaltar.data.database.god

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Entity(tableName = "gods")
data class GodDb(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,

    @ColumnInfo(name = "name")
    val name: String
)