package ru.irikolis.ritualaltar.data.network.gods

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class GodsResponse(
    @Expose
    @SerializedName("god_count")
    val godCount: Int = 0,

    @Expose
    @SerializedName("gods")
    val gods: List<GodResponse> = ArrayList()
)