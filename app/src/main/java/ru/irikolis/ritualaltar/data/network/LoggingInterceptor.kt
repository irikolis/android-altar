package ru.irikolis.ritualaltar.data.network

import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import ru.irikolis.ritualaltar.BuildConfig
import java.io.IOException

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class LoggingInterceptor : Interceptor {
    private val interceptor: Interceptor = HttpLoggingInterceptor()
        .setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        return interceptor.intercept(chain)
    }
}