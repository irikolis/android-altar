package ru.irikolis.ritualaltar.data.repositories

import io.reactivex.Single
import ru.irikolis.ritualaltar.data.database.AppDatabase
import ru.irikolis.ritualaltar.data.mappers.BeaconMapper
import ru.irikolis.ritualaltar.data.mappers.CollectedBeaconMapper
import ru.irikolis.ritualaltar.data.network.ApiService
import ru.irikolis.ritualaltar.domain.models.Beacon
import ru.irikolis.ritualaltar.domain.models.CollectedBeacon

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class BeaconRepository(
    private val apiService: ApiService,
    private val appDatabase: AppDatabase
) {

    fun getBeaconsFromApi(): Single<List<Beacon>> {
        return apiService.getBeacons()
            .flattenAsObservable { items -> items }
            .map(BeaconMapper::networkToEntity)
            .toList()
    }

    fun getBeaconsFromDb(): Single<List<Beacon>> {
        return Single.fromCallable { appDatabase.getBeaconDao().getAll() }
            .flattenAsObservable { items -> items }
            .map(BeaconMapper::dbToEntity)
            .toList()
    }

    fun resetBeaconsDb(beacons: List<Beacon>): Single<List<Long>> {
        return Single.fromCallable { appDatabase.clearTable("beacons") }
            .map { beacons }
            .flattenAsObservable { items -> items }
            .map(BeaconMapper::entityToDb)
            .map { beacon -> appDatabase.getBeaconDao().insert(beacon) }
            .toList()
    }

    fun getCollectedBeaconsFromDb(): Single<List<CollectedBeacon>> {
        return Single.fromCallable { appDatabase.getCollectedBeaconDao().getAll() }
            .flattenAsObservable { items -> items }
            .map(CollectedBeaconMapper::dbToEntity)
            .toList()
    }

    fun clearCollectedBeaconsDb(): Single<Boolean> {
        return Single.fromCallable { appDatabase.clearTable("collected_beacons") }
    }

    fun putCollectedBeaconDb(collectedBeacon: CollectedBeacon): Single<Long> {
        return Single.just(collectedBeacon)
            .map(CollectedBeaconMapper::entityToDb)
            .map { beacon -> appDatabase.getCollectedBeaconDao().insert(beacon) }
    }
}