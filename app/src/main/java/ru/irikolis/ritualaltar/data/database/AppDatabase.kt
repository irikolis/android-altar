package ru.irikolis.ritualaltar.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.irikolis.ritualaltar.data.database.beacon.BeaconDao
import ru.irikolis.ritualaltar.data.database.beacon.BeaconDb
import ru.irikolis.ritualaltar.data.database.collected.CollectedBeaconDao
import ru.irikolis.ritualaltar.data.database.collected.CollectedBeaconDb
import ru.irikolis.ritualaltar.data.database.god.GodDao
import ru.irikolis.ritualaltar.data.database.god.GodDb
import ru.irikolis.ritualaltar.data.database.history.ResultHistoryDao
import ru.irikolis.ritualaltar.data.database.history.ResultHistoryDb
import ru.irikolis.ritualaltar.data.database.ritual.RitualDao
import ru.irikolis.ritualaltar.data.database.ritual.RitualDb

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Database(
    version = 1,
    exportSchema = false,
    entities = [
        GodDb::class,
        RitualDb::class,
        BeaconDb::class,
        CollectedBeaconDb::class,
        ResultHistoryDb::class
    ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getGodDao(): GodDao
    abstract fun getRitualDao(): RitualDao
    abstract fun getBeaconDao(): BeaconDao
    abstract fun getCollectedBeaconDao(): CollectedBeaconDao
    abstract fun getResultHistoryDao(): ResultHistoryDao

    open fun clearTable(tableName: String): Boolean {
        val db = openHelper.writableDatabase
        db.execSQL("DELETE FROM $tableName")
        db.execSQL("UPDATE sqlite_sequence SET seq = 0 WHERE name = '$tableName'")
        return true
    }

    override fun clearAllTables() {
        val tables = arrayOf(
            "gods",
            "rituals",
            "beacons",
            "collected_beacons",
            "result_history"
        )

        for (table in tables)
            clearTable(table)
    }
}