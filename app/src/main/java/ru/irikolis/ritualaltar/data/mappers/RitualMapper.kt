package ru.irikolis.ritualaltar.data.mappers

import ru.irikolis.ritualaltar.data.database.history.ResultHistoryDb
import ru.irikolis.ritualaltar.data.database.ritual.RitualDb
import ru.irikolis.ritualaltar.data.network.activate.ActivateRitualResponse
import ru.irikolis.ritualaltar.data.network.preritual.PreRitualResponse
import ru.irikolis.ritualaltar.data.network.rituals.RitualResponse
import ru.irikolis.ritualaltar.domain.models.ActivateRitual
import ru.irikolis.ritualaltar.domain.models.PreRitual
import ru.irikolis.ritualaltar.domain.models.Ritual

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
object RitualMapper {
    fun networkToEntity(response: RitualResponse) =
        Ritual(
            response.ritualId,
            response.godId,
            response.ritualName!!,
            response.priestCnt,
            response.seniorPriestCnt,
            response.resources!!,
            response.effect!!
        )

    fun dbToEntity(ritualDb: RitualDb) =
        Ritual(
            ritualDb.ritualId,
            ritualDb.godId,
            ritualDb.ritualName,
            ritualDb.priestCnt,
            ritualDb.seniorPriestCnt,
            ritualDb.resources,
            ritualDb.effect
        )

    fun entityToDb(ritual: Ritual) =
        RitualDb(
            ritual.ritualId,
            ritual.godId,
            ritual.name,
            ritual.priestCnt,
            ritual.seniorPriestCnt,
            ritual.resources,
            ritual.effect
        )

    fun networkToEntity(response: ActivateRitualResponse) =
        ActivateRitual(
            response.result!!
        )

    fun networkToEntity(response: PreRitualResponse) =
        PreRitual(
            response.result!!
        )

    fun entityToDb(ritualResult: String) =
        ResultHistoryDb(
            result = ritualResult
        )
}
