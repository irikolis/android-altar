package ru.irikolis.ritualaltar

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
object AppConstants {
    internal const val APP_PREFERENCES = "app_prefs"
    internal const val APP_DATABASE = "app_database"
    internal const val BASE_URL = "84.201.145.244:8000"

    internal const val NETWORK_TIMEOUT_SEC: Long = 5
}