@file:Suppress("unused")

package ru.irikolis.ritualaltar.utils

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class SchedulersProvider {
    fun io() = Schedulers.io()
    fun ui() = AndroidSchedulers.mainThread()
}