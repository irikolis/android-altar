package ru.irikolis.ritualaltar.utils

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
val IP_ADDRESS = Regex("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(:(\\d{1,5})?)?)?)?)?)?)?)?")