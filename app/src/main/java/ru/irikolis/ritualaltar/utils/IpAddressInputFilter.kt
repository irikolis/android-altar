@file:Suppress("unused")

package ru.irikolis.ritualaltar.utils

import android.text.InputFilter
import android.text.Spanned

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class IpAddressInputFilter : InputFilter {
    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        if (end > start) {
            val destTxt = dest.toString()
            val resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend)
            if (resultingTxt.matches(IP_ADDRESS)) {
                val ipEndIndex = resultingTxt.indexOf(':')

                // IP
                val ipTxt = if (ipEndIndex < 0) resultingTxt else resultingTxt.substring(0, ipEndIndex)
                val splits = ipTxt.split('.')
                for (split in splits) {
                    if (split.isNotEmpty() && split.toInt() > 255)
                        return ""
                }

                // Port
                if (ipEndIndex > 0) {
                    val portTxt = resultingTxt.substring(ipEndIndex + 1)
                    if (portTxt.isNotEmpty() && portTxt.toInt() > 65535)
                        return ""
                }
            } else {
                return ""
            }
        }
        return null
    }
}