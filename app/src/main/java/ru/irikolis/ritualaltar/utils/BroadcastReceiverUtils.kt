@file:Suppress("unused")

package ru.irikolis.ritualaltar.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import io.reactivex.Observable
import io.reactivex.ObservableEmitter

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
fun createBroadcastReceiver(context: Context, intentFilter: IntentFilter): Observable<Intent?> {
    val appContext = context.applicationContext
    return Observable.create { emitter: ObservableEmitter<Intent?> ->
        val receiver: BroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intentFilter.hasAction(intent.action)) {
                    emitter.onNext(intent)
                }
            }
        }
        appContext.registerReceiver(receiver, intentFilter)
        emitter.setCancellable { appContext.unregisterReceiver(receiver) }
    }
}
