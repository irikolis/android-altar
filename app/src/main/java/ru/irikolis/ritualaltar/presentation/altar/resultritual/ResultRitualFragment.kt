package ru.irikolis.ritualaltar.presentation.altar.resultritual

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.setFragmentResultListener
import ru.irikolis.ritualaltar.BR
import ru.irikolis.ritualaltar.R
import ru.irikolis.ritualaltar.RitualAltarApplication
import ru.irikolis.ritualaltar.databinding.FragmentResultRitualBinding
import ru.irikolis.ritualaltar.domain.models.RitualResultType
import ru.irikolis.ritualaltar.presentation.altar.AltarActivity
import ru.irikolis.ritualaltar.presentation.altar.preritual.PreRitualFragment
import ru.irikolis.ritualaltar.presentation.core.view.BaseFragment
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class ResultRitualFragment : BaseFragment<FragmentResultRitualBinding, ResultRitualViewModel>() {

    companion object {
        @JvmStatic
        fun newInstance() = ResultRitualFragment()
    }

    override val layoutId = R.layout.fragment_result_ritual
    override val bindingVariable = BR.model

    @Inject
    override lateinit var model: ResultRitualViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        RitualAltarApplication.application.components.getResultRitualComponent(this).inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as? AltarActivity)?.title = getString(R.string.ritual_result)

        model.ritualResult.observe(viewLifecycleOwner) { binding.textViewRitualResult.text = it }
        model.loadingErrorCommand.observe(this) { showLoadingError() }

        setFragmentResultListener(PreRitualFragment.KEY_REQUEST) { _, bundle ->
            binding.buttonClose.visibility =
                if (bundle.getString(PreRitualFragment.KEY_RESULT_TYPE) == RitualResultType.PREVIEW_RITUAL.name)
                    View.GONE else View.VISIBLE
        }
        model.navigateCommand.observe(this) { navigateToNext() }
    }

    private fun showLoadingError() {
        Toast.makeText(context, R.string.loading_error, Toast.LENGTH_LONG).show()
    }

    private fun navigateToNext() {
        (requireActivity() as? AltarActivity)?.navigateToNextFragment(layoutId)
    }

    override fun onDestroy() {
        super.onDestroy()
        RitualAltarApplication.application.components.clearResultRitualComponent()
    }
}