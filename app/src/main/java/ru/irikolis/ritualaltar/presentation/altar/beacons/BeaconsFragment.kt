package ru.irikolis.ritualaltar.presentation.altar.beacons

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
import ru.irikolis.ritualaltar.BR
import ru.irikolis.ritualaltar.R
import ru.irikolis.ritualaltar.RitualAltarApplication
import ru.irikolis.ritualaltar.databinding.FragmentBeaconsBinding
import ru.irikolis.ritualaltar.presentation.altar.AltarActivity

import ru.irikolis.ritualaltar.presentation.core.view.BaseFragment
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class BeaconsFragment : BaseFragment<FragmentBeaconsBinding, BeaconsViewModel>() {

    companion object {
        @JvmStatic
        fun newInstance() = BeaconsFragment()
    }

    override val layoutId = R.layout.fragment_beacons
    override val bindingVariable = BR.model

    @Inject
    override lateinit var model: BeaconsViewModel

    @Inject
    lateinit var adapter: BeaconsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        RitualAltarApplication.application.components.getBeaconsComponent(this).inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as? AltarActivity)?.title = getString(R.string.priests_and_artifacts)

        binding.recyclerViewBeacons.addItemDecoration(
            DividerItemDecoration(binding.recyclerViewBeacons.context, VERTICAL)
        )
        binding.recyclerViewBeacons.adapter = adapter
        binding.swipeRefreshBeacons.setOnRefreshListener { model.detectBeacons() }

        model.beacons.observe(viewLifecycleOwner, { adapter.setBeacons(it) })
        model.ritualsCommand.observe(this) {
            (requireActivity() as? AltarActivity)?.navigateToNextFragment(layoutId)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        RitualAltarApplication.application.components.clearBeaconsComponent()
    }
}
