package ru.irikolis.ritualaltar.presentation.altar.rituals

import ru.irikolis.ritualaltar.data.preference.AppPreference
import ru.irikolis.ritualaltar.domain.interactors.BeaconInteractor
import ru.irikolis.ritualaltar.domain.interactors.RitualInteractor
import ru.irikolis.ritualaltar.domain.models.Beacon
import ru.irikolis.ritualaltar.domain.models.BeaconType
import ru.irikolis.ritualaltar.domain.models.Ritual
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewModel
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewState
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewCommand
import ru.irikolis.ritualaltar.utils.SchedulersProvider
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class RitualsViewModel @Inject constructor(
    private val appPreference: AppPreference,
    private val ritualInteractor: RitualInteractor,
    private val beaconInteractor: BeaconInteractor,
    private val schedulersProvider: SchedulersProvider
) : BaseViewModel() {

    val beacons = BaseViewState(listOf<Beacon>())
    val rituals = BaseViewState(listOf<Ritual>())
    val isEmptyList = BaseViewState(false)
    val isRefreshing = BaseViewState(false)

    val navigateCommand = BaseViewCommand<Void>()

    init {
        loadBeaconsAndRituals()
    }

    fun loadBeaconsAndRituals() {
        val disposable = beaconInteractor.getBeacons()
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { isRefreshing.value = true }
            .doOnError { isRefreshing.value = false }
            .subscribe ({
                beacons.value = it
                loadRituals()
            }, {
            })
        unsubscribeOnDestroy(disposable)
    }

    private fun loadRituals() {
        val disposable = ritualInteractor.getRituals()
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { isRefreshing.value = true }
            .doAfterTerminate { isRefreshing.value = false }
            .subscribe ({
                isEmptyList.value = false
                rituals.value = it.filterLoadedData()
            }, {
                isEmptyList.value = true
            })
        unsubscribeOnDestroy(disposable)
    }

    private fun List<Ritual>.filterLoadedData() =
        this.filter { it.godId == appPreference.godId }
        .filter { it.priestCnt <= beacons.value.count { beacon -> beacon.type == BeaconType.PRIEST } }
        .filter { it.seniorPriestCnt <= beacons.value.count { beacon -> beacon.type == BeaconType.SENIOR_PRIEST } }

    fun onItemClick(ritual: Ritual) {
        appPreference.ritualId = ritual.ritualId
        navigateCommand.call()
    }
}