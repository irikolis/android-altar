package ru.irikolis.ritualaltar.presentation.authorization

import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentPagerAdapter
import ru.irikolis.ritualaltar.BR
import ru.irikolis.ritualaltar.R
import ru.irikolis.ritualaltar.RitualAltarApplication
import ru.irikolis.ritualaltar.databinding.ActivityAuthBinding
import ru.irikolis.ritualaltar.presentation.altar.AltarActivity
import ru.irikolis.ritualaltar.presentation.authorization.login.LoginFragment
import ru.irikolis.ritualaltar.presentation.authorization.pincode.PinCodeFragment
import ru.irikolis.ritualaltar.presentation.core.view.BaseActivity
import ru.irikolis.ritualaltar.presentation.main.MainActivity
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class AuthActivity : BaseActivity<ActivityAuthBinding, AuthViewModel>() {

    override val layoutId: Int = R.layout.activity_auth
    override val bindingVariable: Int = BR.model

    @Inject
    override lateinit var model: AuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        RitualAltarApplication.application.components.getAuthComponent(this).inject(this)
        super.onCreate(savedInstanceState)

        @Suppress("USELESS_CAST")
        (binding.toolbarBack as Toolbar).apply {
            setTitle(R.string.autorization)
            setNavigationOnClickListener { navigateBack() }
        }

        setupViewPager()
        binding.tabLayoutAuth.setupWithViewPager(binding.viewPagerAuth)
    }

    private fun setupViewPager() {
        val adapter = AuthViewPagerAdapter(
            supportFragmentManager,
            FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        )
        adapter.addFragment(LoginFragment.newInstance(), getString(R.string.login))
        adapter.addFragment(PinCodeFragment.newInstance(), getString(R.string.pin_code))
        binding.viewPagerAuth.adapter = adapter
    }

    private fun navigateBack() {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
    }

    fun navigateToAltar() {
        val intent = Intent(this, AltarActivity::class.java)
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        RitualAltarApplication.application.components.clearAuthComponent()
    }
}