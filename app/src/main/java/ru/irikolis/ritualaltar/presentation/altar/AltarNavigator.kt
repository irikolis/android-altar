package ru.irikolis.ritualaltar.presentation.altar

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import org.jetbrains.annotations.NotNull
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class AltarNavigator(activity: @NotNull FragmentActivity, containerId: Int) :
    SupportAppNavigator(activity, containerId) {

    override fun setupFragmentTransaction(
        command: Command,
        currentFragment: Fragment?,
        nextFragment: Fragment?,
        fragmentTransaction: FragmentTransaction
    ) {
        super.setupFragmentTransaction(command, currentFragment, nextFragment, fragmentTransaction)
    }
}