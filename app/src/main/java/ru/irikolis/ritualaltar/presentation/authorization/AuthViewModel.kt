package ru.irikolis.ritualaltar.presentation.authorization

import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewModel

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class AuthViewModel : BaseViewModel()