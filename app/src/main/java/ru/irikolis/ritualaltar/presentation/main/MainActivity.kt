package ru.irikolis.ritualaltar.presentation.main

import android.content.Intent
import android.os.Bundle
import ru.irikolis.ritualaltar.BR
import ru.irikolis.ritualaltar.R
import ru.irikolis.ritualaltar.RitualAltarApplication
import ru.irikolis.ritualaltar.databinding.ActivityMainBinding
import ru.irikolis.ritualaltar.presentation.authorization.AuthActivity
import ru.irikolis.ritualaltar.presentation.core.view.BaseActivity
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    override val layoutId: Int = R.layout.activity_main
    override val bindingVariable: Int = BR.model

    @Inject
    override lateinit var model: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        RitualAltarApplication.application.components.getMainComponent(this).inject(this)
        super.onCreate(savedInstanceState)

        model.altarCommand.observe(this) { navigateAuth() }
    }

    private fun navigateAuth() {
        val intent = Intent(this, AuthActivity::class.java)
        startActivity(intent)
    }
    override fun onDestroy() {
        super.onDestroy()
        RitualAltarApplication.application.components.clearMainComponent()
    }
}
