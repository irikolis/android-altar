package ru.irikolis.ritualaltar.presentation.altar

import ru.irikolis.ritualaltar.R
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewModel
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewCommand
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class AltarViewModel @Inject constructor() : BaseViewModel() {

    val fragmentChangeCommand = BaseViewCommand<Int>()

    init {
        fragmentChangeCommand.call(R.layout.fragment_gods)
    }

    fun onNavigateClick(currentFragmentId: Int) {
        val nextId = when (currentFragmentId) {
            R.layout.fragment_gods -> R.layout.fragment_beacons
            R.layout.fragment_beacons -> R.layout.fragment_rituals
            R.layout.fragment_rituals -> R.layout.fragment_pre_ritual
            R.layout.fragment_pre_ritual -> R.layout.fragment_result_ritual
            R.layout.fragment_result_ritual -> R.layout.fragment_gods
            else -> null
        }
        nextId?.let { fragmentChangeCommand.call(it) }
    }
}