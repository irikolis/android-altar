package ru.irikolis.ritualaltar.presentation.authorization

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import java.util.*

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class AuthViewPagerAdapter(
    fm: FragmentManager,
    behavior: Int,
    private val fragments: MutableList<Fragment> = ArrayList(),
    private val titles: MutableList<String> = ArrayList()
) : FragmentPagerAdapter(fm, behavior) {

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getPageTitle(position: Int): CharSequence {
        return titles[position]
    }

    fun addFragment(fragment: Fragment, title: String) {
        fragments.add(fragment)
        titles.add(title)
    }
}