package ru.irikolis.ritualaltar.presentation.altar.rituals

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
import ru.irikolis.ritualaltar.BR
import ru.irikolis.ritualaltar.R
import ru.irikolis.ritualaltar.RitualAltarApplication
import ru.irikolis.ritualaltar.databinding.FragmentRitualsBinding
import ru.irikolis.ritualaltar.presentation.altar.AltarActivity

import ru.irikolis.ritualaltar.presentation.core.view.BaseFragment
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class RitualsFragment : BaseFragment<FragmentRitualsBinding, RitualsViewModel>() {

    companion object {
        @JvmStatic
        fun newInstance() = RitualsFragment()
    }

    override val layoutId = R.layout.fragment_rituals
    override val bindingVariable = BR.model

    @Inject
    override lateinit var model: RitualsViewModel

    @Inject
    lateinit var adapter: RitualsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        RitualAltarApplication.application.components.getRitualsComponent(this).inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as? AltarActivity)?.title = getString(R.string.rituals)

        binding.recyclerViewRituals.addItemDecoration(
            DividerItemDecoration(binding.recyclerViewRituals.context, VERTICAL)
        )
        adapter.onItemClickListener = { model.onItemClick(it) }
        binding.recyclerViewRituals.adapter = adapter
        binding.swipeRefreshRitual.setOnRefreshListener { model.loadBeaconsAndRituals() }

        model.rituals.observe(viewLifecycleOwner, { adapter.setRituals(it) })
        model.navigateCommand.observe(this) { navigateToNext() }
    }

    private fun navigateToNext() {
        (requireActivity() as? AltarActivity)?.navigateToNextFragment(layoutId)
    }

    override fun onDestroy() {
        super.onDestroy()
        RitualAltarApplication.application.components.clearRitualsComponent()
    }
}
