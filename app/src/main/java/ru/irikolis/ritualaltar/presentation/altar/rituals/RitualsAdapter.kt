package ru.irikolis.ritualaltar.presentation.altar.rituals

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ru.irikolis.ritualaltar.databinding.ItemRitualBinding
import ru.irikolis.ritualaltar.domain.models.Ritual

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class RitualsAdapter(
    private val model: RitualsViewModel,
    var onItemClickListener: ((Ritual) -> Unit)? = null
) : RecyclerView.Adapter<RitualsAdapter.ViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<Ritual>() {
        override fun areItemsTheSame(oldItem: Ritual, newItem: Ritual) = oldItem.ritualId == newItem.ritualId

        override fun areContentsTheSame(oldItem: Ritual, newItem: Ritual) = oldItem == newItem
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemRitualBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ViewHolder(binding)
        binding.model = model
        binding.root.setOnClickListener {
            onItemClickListener?.let {
                val position = holder.adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    it.invoke(getItem(position))
                }
            }
        }
        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.model = model
        holder.binding.data = getItem(position)
    }

    override fun getItemCount(): Int = differ.currentList.size

    private fun getItem(position: Int) = differ.currentList[position]

    class ViewHolder(val binding: ItemRitualBinding) : RecyclerView.ViewHolder(binding.root)

    fun setRituals(rituals: List<Ritual>) {
        differ.submitList(rituals)
    }
}