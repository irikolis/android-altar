package ru.irikolis.ritualaltar.presentation.altar

import androidx.fragment.app.Fragment
import ru.irikolis.ritualaltar.presentation.altar.beacons.BeaconsFragment
import ru.irikolis.ritualaltar.presentation.altar.gods.GodsFragment
import ru.irikolis.ritualaltar.presentation.altar.preritual.PreRitualFragment
import ru.irikolis.ritualaltar.presentation.altar.resultritual.ResultRitualFragment
import ru.irikolis.ritualaltar.presentation.altar.rituals.RitualsFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

/**
 * @author Irina Kolovorotnaya (irikolis)
 */

class GodsScreen : SupportAppScreen() {
    override fun getFragment(): Fragment = GodsFragment.newInstance()
}

class BeaconsScreen : SupportAppScreen() {
    override fun getFragment(): Fragment = BeaconsFragment.newInstance()
}

class RitualsScreen : SupportAppScreen() {
    override fun getFragment(): Fragment = RitualsFragment.newInstance()
}

class PreRitualScreen : SupportAppScreen() {
    override fun getFragment(): Fragment = PreRitualFragment.newInstance()
}

class ResultRitualScreen : SupportAppScreen() {
    override fun getFragment(): Fragment = ResultRitualFragment.newInstance()
}