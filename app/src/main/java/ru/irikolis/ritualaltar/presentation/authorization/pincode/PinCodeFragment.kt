package ru.irikolis.ritualaltar.presentation.authorization.pincode

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import ru.irikolis.ritualaltar.BR
import ru.irikolis.ritualaltar.R
import ru.irikolis.ritualaltar.RitualAltarApplication
import ru.irikolis.ritualaltar.databinding.FragmentPinCodeBinding
import ru.irikolis.ritualaltar.presentation.authorization.AuthActivity
import ru.irikolis.ritualaltar.presentation.core.view.BaseFragment
import ru.irikolis.ritualaltar.utils.IpAddressInputFilter
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class PinCodeFragment : BaseFragment<FragmentPinCodeBinding, PinCodeViewModel>() {

    companion object {
        @JvmStatic
        fun newInstance() = PinCodeFragment()
    }

    override val layoutId: Int = R.layout.fragment_pin_code
    override val bindingVariable: Int = BR.model

    @Inject
    override lateinit var model: PinCodeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        RitualAltarApplication.application.components.getPinCodeComponent(this).inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.editTextIpAddress.filters = arrayOf(IpAddressInputFilter())

        model.isLoading.observe(viewLifecycleOwner) { binding.loadingButton.setAnimatedLoading(it)  }

        model.addressValidCommand.observe(this) { setInputDataValid(binding.editTextIpAddress, it!!) }
        model.pinCodeValidCommand.observe(this) { setInputDataValid(binding.editTextPinCode, it!!) }

        model.pinCodeSuccessCommand.observe(this) { (requireActivity() as? AuthActivity)?.navigateToAltar() }
        model.pinCodeErrorCommand.observe(this) { showPinCodeError() }
    }

    private fun showPinCodeError() {
        Toast.makeText(context, R.string.invalid_pin_code, Toast.LENGTH_LONG).show()
    }

    private fun setInputDataValid(edit: EditText, valid: Boolean) {
        context?.let {
            val drawable = if (valid) null else ContextCompat.getDrawable(
                it,
                R.drawable.ic_data_error_24dp
            )
            edit.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        RitualAltarApplication.application.components.clearPinCodeComponent()
    }
}