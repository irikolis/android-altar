package ru.irikolis.ritualaltar.presentation.altar.preritual

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import ru.irikolis.ritualaltar.BR
import ru.irikolis.ritualaltar.R
import ru.irikolis.ritualaltar.RitualAltarApplication
import ru.irikolis.ritualaltar.databinding.FragmentPreRitualBinding
import ru.irikolis.ritualaltar.domain.models.RitualResultType
import ru.irikolis.ritualaltar.presentation.altar.AltarActivity
import ru.irikolis.ritualaltar.presentation.core.view.BaseFragment
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class PreRitualFragment : BaseFragment<FragmentPreRitualBinding, PreRitualViewModel>() {

    companion object {
        const val KEY_RESULT_TYPE = "ritual_result_type"
        const val KEY_REQUEST = "ritual_request"

        @JvmStatic
        fun newInstance() = PreRitualFragment()
    }

    override val layoutId = R.layout.fragment_pre_ritual
    override val bindingVariable = BR.model

    @Inject
    override lateinit var model: PreRitualViewModel

    @Inject
    lateinit var router: Router

    override fun onCreate(savedInstanceState: Bundle?) {
        RitualAltarApplication.application.components.getPreRitualComponent(this).inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as? AltarActivity)?.title = getString(R.string.ritual_preparation)

        model.loadingErrorCommand.observe(this) { showLoadingError() }
        model.previewLoadingCommand.observe(this) { binding.loadingButtonPreview.setAnimatedLoading(it!!) }
        model.activationLoadingCommand.observe(this) { binding.loadingButtonActivate.setAnimatedLoading(it!!) }

        model.previewSuccessCommand.observe(this) {
            setFragmentResult(KEY_REQUEST, bundleOf(KEY_RESULT_TYPE to RitualResultType.PREVIEW_RITUAL.name))
            (requireActivity() as? AltarActivity)?.navigateToNextFragment(layoutId) }
        model.previewErrorCommand.observe(this) { showPreviewError() }

        model.activationSuccessCommand.observe(this) {
            setFragmentResult(KEY_REQUEST, bundleOf(KEY_RESULT_TYPE to RitualResultType.ACTIVATION_RITUAL.name))
            (requireActivity() as? AltarActivity)?.navigateToNextFragment(layoutId)
        }
        model.activationErrorCommand.observe(this) { showActivationError() }
    }

    private fun showLoadingError() {
        Toast.makeText(context, R.string.ritual_loading_error, Toast.LENGTH_LONG).show()
    }

    private fun showPreviewError() {
        Toast.makeText(context, R.string.ritual_preview_error, Toast.LENGTH_LONG).show()
    }

    private fun showActivationError() {
        Toast.makeText(context, R.string.ritual_activation_error, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        RitualAltarApplication.application.components.clearPreRitualComponent()
    }
}