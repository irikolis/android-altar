package ru.irikolis.ritualaltar.presentation.altar.resultritual

import ru.irikolis.ritualaltar.domain.interactors.RitualInteractor
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewModel
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewState
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewCommand
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class ResultRitualViewModel(
    private val ritualInteractor: RitualInteractor,
    private val schedulersProvider: SchedulersProvider
) : BaseViewModel() {

    val ritualResult = BaseViewState("")
    val isLoading = BaseViewState(false)

    val loadingErrorCommand = BaseViewCommand<Void>()
    val navigateCommand = BaseViewCommand<Void>()

    init {
        loadResult()
    }

    private fun loadResult() {
        val disposable = ritualInteractor.getRitualResult()
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { isLoading.value = true }
            .doAfterTerminate { isLoading.value = false }
            .subscribe ({
                ritualResult.value = it
            }, {
                loadingErrorCommand.call()
            })
        unsubscribeOnDestroy(disposable)
    }

    fun onCloseClick() {
        navigateCommand.call()
    }
}