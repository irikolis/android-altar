package ru.irikolis.ritualaltar.presentation.altar

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import ru.irikolis.ritualaltar.BR
import ru.irikolis.ritualaltar.R
import ru.irikolis.ritualaltar.RitualAltarApplication
import ru.irikolis.ritualaltar.databinding.ActivityAltarBinding
import ru.irikolis.ritualaltar.presentation.core.view.BaseActivity
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class AltarActivity : BaseActivity<ActivityAltarBinding, AltarViewModel>() {

    override val layoutId: Int = R.layout.activity_altar
    override val bindingVariable: Int = BR.model

    @Inject
    override lateinit var model: AltarViewModel

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var router: Router

    private lateinit var navigator: AltarNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        RitualAltarApplication.application.components.getAltarComponent(this).inject(this)
        super.onCreate(savedInstanceState)

        @Suppress("USELESS_CAST")
        (binding.toolbar as Toolbar).setNavigationOnClickListener { router.exit() }

        navigator = AltarNavigator(this, R.id.frame_container)
        model.fragmentChangeCommand.observe(this) {
            when (it) {
                R.layout.fragment_gods -> router.newRootScreen(GodsScreen())
                R.layout.fragment_beacons -> router.navigateTo(BeaconsScreen())
                R.layout.fragment_rituals -> router.navigateTo(RitualsScreen())
                R.layout.fragment_pre_ritual -> router.navigateTo(PreRitualScreen())
                R.layout.fragment_result_ritual -> router.navigateTo(ResultRitualScreen())
            }
        }
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun setTitle(title: CharSequence?) {
        @Suppress("USELESS_CAST")
        (binding.toolbar as Toolbar).title = title
    }

    fun navigateToNextFragment(currentFragmentId: Int) {
        model.onNavigateClick(currentFragmentId)
    }

    override fun onDestroy() {
        super.onDestroy()
        RitualAltarApplication.application.components.clearAltarComponent()
    }
}
