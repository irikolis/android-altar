@file:Suppress("unused")

package ru.irikolis.ritualaltar.presentation.core.databinding.adapters

import android.view.View
import androidx.databinding.BindingAdapter

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@BindingAdapter("app:visible")
fun setVisible(view: View, value: Boolean) {
	view.visibility = if (value) View.VISIBLE else View.GONE
}

@BindingAdapter("app:invisible")
fun setInvisible(view: View, value: Boolean) {
	view.visibility = if (value) View.INVISIBLE else View.VISIBLE
}

@BindingAdapter("app:gone")
fun setGone(view: View, value: Boolean) {
	view.visibility = if (value) View.GONE else View.VISIBLE
}

