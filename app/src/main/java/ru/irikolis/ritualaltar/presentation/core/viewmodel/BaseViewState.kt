package ru.irikolis.ritualaltar.presentation.core.viewmodel

import androidx.lifecycle.MutableLiveData

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class BaseViewState<T : Any> : MutableLiveData<T> {

	constructor () : super()
	constructor (defaultValue: T) : super(defaultValue)

	override fun getValue(): T {
		return super.getValue()!!
	}
}