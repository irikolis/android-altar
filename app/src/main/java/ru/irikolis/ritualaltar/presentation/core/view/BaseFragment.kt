package ru.irikolis.ritualaltar.presentation.core.view

import androidx.databinding.ViewDataBinding
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.annotation.LayoutRes
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewModel

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel> : Fragment() {

    protected lateinit var binding: T

    protected abstract val model: V

    @get:LayoutRes
    abstract val layoutId: Int

    abstract val bindingVariable: Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.let {
            it.setVariable(bindingVariable, model)
            it.lifecycleOwner = this
            it.executePendingBindings()
        }
    }
}
