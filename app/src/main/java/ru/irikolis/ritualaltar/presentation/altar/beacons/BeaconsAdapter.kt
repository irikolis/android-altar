package ru.irikolis.ritualaltar.presentation.altar.beacons

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ru.irikolis.ritualaltar.databinding.ItemBeaconBinding
import ru.irikolis.ritualaltar.domain.models.Beacon

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class BeaconsAdapter(
    private val model: BeaconsViewModel,
    private var onItemClickListener: ((Beacon) -> Unit)? = null
) : RecyclerView.Adapter<BeaconsAdapter.ViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<Beacon>() {
        override fun areItemsTheSame(oldItem: Beacon, newItem: Beacon) = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Beacon, newItem: Beacon) = oldItem == newItem
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemBeaconBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ViewHolder(binding)
        binding.model = model
        binding.root.setOnClickListener {
            onItemClickListener?.let {
                val position = holder.adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    it.invoke(getItem(position))
                }
            }
        }
        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.model = model
        holder.binding.data = getItem(position)
    }

    override fun getItemCount(): Int = differ.currentList.size

    private fun getItem(position: Int) = differ.currentList[position]

    class ViewHolder(val binding: ItemBeaconBinding) : RecyclerView.ViewHolder(binding.root)

    fun setBeacons(beacons: List<Beacon>) {
        differ.submitList(beacons)
    }
}