package ru.irikolis.ritualaltar.presentation.core.viewmodel

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import java.util.concurrent.atomic.AtomicBoolean

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class BaseViewCommand<T> : MutableLiveData<T>() {

	val eventValue: T
		get() = value!!

	private val pending = AtomicBoolean(false)

	@MainThread
	override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
		observe(owner, observer::onChanged)
	}

	@MainThread
	fun observe(owner: LifecycleOwner, observer: (T?) -> Unit) {
		super.observe(owner, { t ->
			if (pending.compareAndSet(true, false)) {
				observer(t)
			}
		})
	}

	@MainThread
	override fun setValue(t: T?) {
		pending.set(true)
		super.setValue(t)
	}

	@MainThread
	fun call() {
		value = null
	}

	@MainThread
	fun call(value: T) {
		this.value = value
	}
}
