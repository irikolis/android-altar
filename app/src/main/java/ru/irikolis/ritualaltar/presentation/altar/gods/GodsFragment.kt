package ru.irikolis.ritualaltar.presentation.altar.gods

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
import ru.irikolis.ritualaltar.BR
import ru.irikolis.ritualaltar.R
import ru.irikolis.ritualaltar.RitualAltarApplication
import ru.irikolis.ritualaltar.databinding.FragmentGodsBinding
import ru.irikolis.ritualaltar.presentation.altar.AltarActivity
import ru.irikolis.ritualaltar.presentation.core.view.BaseFragment
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class GodsFragment : BaseFragment<FragmentGodsBinding, GodsViewModel>() {

    companion object {
        @JvmStatic
        fun newInstance() = GodsFragment()
    }

    override val layoutId = R.layout.fragment_gods
    override val bindingVariable = BR.model

    @Inject
    override lateinit var model: GodsViewModel

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var adapter: GodsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        RitualAltarApplication.application.components.getGodsComponent(this).inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as? AltarActivity)?.title = getString(R.string.gods)

        binding.recyclerViewGods.addItemDecoration(
            DividerItemDecoration(binding.recyclerViewGods.context, VERTICAL)
        )
        adapter.onItemClickListener = { model.onItemClick(it) }
        binding.recyclerViewGods.adapter = adapter
        binding.swipeRefreshRoute.setOnRefreshListener { model.loadGods() }

        model.gods.observe(viewLifecycleOwner, { adapter.setGods(it) })
        model.navigateCommand.observe(this) { navigateToNext() }
    }

    private fun navigateToNext() {
        (requireActivity() as? AltarActivity)?.navigateToNextFragment(layoutId)
    }

    override fun onDestroy() {
        super.onDestroy()
        RitualAltarApplication.application.components.clearGodsComponent()
    }
}
