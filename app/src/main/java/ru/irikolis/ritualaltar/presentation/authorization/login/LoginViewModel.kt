package ru.irikolis.ritualaltar.presentation.authorization.login

import ru.irikolis.ritualaltar.data.preference.AppPreference
import ru.irikolis.ritualaltar.domain.interactors.AuthInteractor
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewCommand
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewModel
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewState
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class LoginViewModel(
    private val appPreference: AppPreference,
    private val authInteractor: AuthInteractor,
    private val schedulersProvider: SchedulersProvider
) : BaseViewModel() {

    val address = BaseViewState(appPreference.baseUrl)
    val username = BaseViewState("")
    val password = BaseViewState("")

    val isLoading = BaseViewState(false)

    val addressValidCommand = BaseViewCommand<Boolean>()
    val usernameValidCommand = BaseViewCommand<Boolean>()
    val passwordValidCommand = BaseViewCommand<Boolean>()

    val loginSuccessCommand = BaseViewCommand<Void>()
    val loginErrorCommand = BaseViewCommand<Void>()

    fun onAuthClick() {
        if (isInputDataValid(address.value, username.value, password.value)) {
            appPreference.baseUrl = address.value
            val disposable = authInteractor.performAuthLogin(username.value, password.value)
                .observeOn(schedulersProvider.ui())
                .doOnSubscribe { isLoading.value = true }
                .doOnEvent { _, _ ->  isLoading.value = false }
                .subscribe({
                    loginSuccessCommand.call()
                }, {
                    loginErrorCommand.call()
                })
            unsubscribeOnDestroy(disposable)
        }
    }

    private fun isInputDataValid(address: String, username: String, password: String): Boolean {
        addressValidCommand.call(address.isNotEmpty())
        usernameValidCommand.call(username.isNotEmpty())
        passwordValidCommand.call(password.isNotEmpty())
        return addressValidCommand.eventValue && usernameValidCommand.eventValue && passwordValidCommand.eventValue
    }
}