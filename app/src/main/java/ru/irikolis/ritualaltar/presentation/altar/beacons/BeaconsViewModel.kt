package ru.irikolis.ritualaltar.presentation.altar.beacons

import ru.irikolis.ritualaltar.domain.interactors.BeaconInteractor
import ru.irikolis.ritualaltar.domain.models.Beacon
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewModel
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewCommand
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewState
import ru.irikolis.ritualaltar.utils.SchedulersProvider
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class BeaconsViewModel @Inject constructor(
    private val beaconInteractor: BeaconInteractor,
    private val schedulersProvider: SchedulersProvider
) : BaseViewModel() {

    val beacons = BaseViewState<MutableList<Beacon>>(mutableListOf())
    val isEmptyList = BaseViewState(false)
    val isRefreshing = BaseViewState(false)

    val ritualsCommand = BaseViewCommand<ArrayList<Beacon>>()

    init {
        detectBeacons()
    }

    fun detectBeacons() {
        val disposable = beaconInteractor.detectBeacons()
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { isRefreshing.value = true }
            .doAfterTerminate { isRefreshing.value = false }
            .subscribe ({
                isEmptyList.value = false
                beacons.value = ArrayList(it)
            }, {
                isEmptyList.value = true
            })
        unsubscribeOnDestroy(disposable)
    }

    fun onRitualsClick() {
        ritualsCommand.call(beacons.value as ArrayList<Beacon>)
    }
}