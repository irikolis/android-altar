package ru.irikolis.ritualaltar.presentation.altar.gods

import ru.irikolis.ritualaltar.data.preference.AppPreference
import ru.irikolis.ritualaltar.domain.interactors.GodInteractor
import ru.irikolis.ritualaltar.domain.models.God
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewModel
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewCommand
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewState
import ru.irikolis.ritualaltar.utils.SchedulersProvider
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class GodsViewModel @Inject constructor(
    private val appPreference: AppPreference,
    private val godInteractor: GodInteractor,
    private val schedulersProvider: SchedulersProvider
) : BaseViewModel() {

    val gods = BaseViewState<MutableList<God>>(mutableListOf())
    val isEmptyList = BaseViewState(false)
    val isRefreshing = BaseViewState(false)

    val navigateCommand = BaseViewCommand<Void>()

    init {
        loadGods()
    }

    fun loadGods() {
        val disposable = godInteractor.getGods()
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { isRefreshing.value = true }
            .doAfterTerminate { isRefreshing.value = false }
            .subscribe ({
                isEmptyList.value = false
                gods.value = ArrayList(it)
            }, {
                isEmptyList.value = true
            })
        unsubscribeOnDestroy(disposable)
    }

    fun onItemClick(god: God) {
        appPreference.godId = god.godId
        navigateCommand.call()
    }
}