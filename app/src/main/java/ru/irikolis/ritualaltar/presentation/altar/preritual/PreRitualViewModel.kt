package ru.irikolis.ritualaltar.presentation.altar.preritual

import ru.irikolis.ritualaltar.data.preference.AppPreference
import ru.irikolis.ritualaltar.domain.interactors.BeaconInteractor
import ru.irikolis.ritualaltar.domain.interactors.RitualInteractor
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewModel
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewState
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewCommand
import ru.irikolis.ritualaltar.utils.SchedulersProvider
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class PreRitualViewModel @Inject constructor(
    private val appPreference: AppPreference,
    private val ritualInteractor: RitualInteractor,
    private val beaconInteractor: BeaconInteractor,
    private val schedulersProvider: SchedulersProvider
) : BaseViewModel() {

    private var beaconIds = BaseViewState(intArrayOf())

    var ritualName = BaseViewState("")
    var resources = BaseViewState("")
    var description = BaseViewState("")

    val wageringQuality = BaseViewState(3)
    val diceRoll = BaseViewState(10)

    val isRitualLoading = BaseViewState(false)
    val isPreviewLoading = BaseViewState(false)
    val isActivationLoading = BaseViewState(false)

    val loadingErrorCommand = BaseViewCommand<Void>()

    val previewLoadingCommand = BaseViewCommand<Boolean>()
    val previewSuccessCommand = BaseViewCommand<String>()
    val previewErrorCommand = BaseViewCommand<Void>()

    val activationLoadingCommand = BaseViewCommand<Boolean>()
    val activationSuccessCommand = BaseViewCommand<String>()
    val activationErrorCommand = BaseViewCommand<Void>()

    init {
        loadBeaconIdsAndRitual()
    }

    private fun loadBeaconIdsAndRitual() {
        val disposable = beaconInteractor.getBeacons()
            .map { beacons -> beacons.map { it.id } }
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { isRitualLoading.value = true }
            .doOnError { isRitualLoading.value = false }
            .subscribe ({
                beaconIds.value = it.toIntArray()
                loadRitual()
            }, {
            })
        unsubscribeOnDestroy(disposable)
    }

    private fun loadRitual() {
        val disposable = ritualInteractor.getRitual(appPreference.ritualId)
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe { isRitualLoading.value = true }
            .doAfterTerminate { isRitualLoading.value = false }
            .subscribe ({
                ritualName.value = it.name
                resources.value = it.resources
                description.value = it.effect
            }, {
                loadingErrorCommand.call()
            })
        unsubscribeOnDestroy(disposable)
    }

    fun onRitualPreviewClick() {
        val disposable = ritualInteractor.performPreRitual(
                appPreference.ritualId,
                beaconIds.value,
                wageringQuality.value,
                diceRoll.value
            )
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe {
                isPreviewLoading.value = true
                previewLoadingCommand.call(true)
            }
            .doOnEvent { _, _ ->
                isPreviewLoading.value = false
                previewLoadingCommand.call(false)
            }
            .subscribe ({
                previewSuccessCommand.call(it.result)
            }, {
                previewErrorCommand.call()
            })
        unsubscribeOnDestroy(disposable)
    }

    fun onRitualActivateClick() {
        val disposable = ritualInteractor.performActivateRitual(
                appPreference.ritualId,
                beaconIds.value,
                wageringQuality.value,
                diceRoll.value
            )
            .observeOn(schedulersProvider.ui())
            .doOnSubscribe {
                isActivationLoading.value = true
                activationLoadingCommand.call(true)
            }
            .doOnEvent { _, _ ->
                isActivationLoading.value = false
                activationLoadingCommand.call(false)
            }
            .subscribe ({
                activationSuccessCommand.call(it.result)
            }, {
                activationErrorCommand.call()
            })
        unsubscribeOnDestroy(disposable)
    }
}