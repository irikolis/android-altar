package ru.irikolis.ritualaltar.presentation.main

import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewModel
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewCommand

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class MainViewModel : BaseViewModel() {
    val altarCommand = BaseViewCommand<Void>()

    fun onAltarClick() {
        altarCommand.call()
    }
}