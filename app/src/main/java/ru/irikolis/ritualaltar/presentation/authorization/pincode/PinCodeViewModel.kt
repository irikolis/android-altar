package ru.irikolis.ritualaltar.presentation.authorization.pincode

import ru.irikolis.ritualaltar.data.preference.AppPreference
import ru.irikolis.ritualaltar.domain.interactors.AuthInteractor
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewModel
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewState
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewCommand
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class PinCodeViewModel(
    val appPreference: AppPreference,
    val authInteractor: AuthInteractor,
    val schedulersProvider: SchedulersProvider
) : BaseViewModel() {

    var address = BaseViewState(appPreference.baseUrl)
    var pinCode = BaseViewState("")

    var isLoading = BaseViewState(false)

    val addressValidCommand = BaseViewCommand<Boolean>()
    val pinCodeValidCommand = BaseViewCommand<Boolean>()

    val pinCodeSuccessCommand = BaseViewCommand<Void>()
    val pinCodeErrorCommand = BaseViewCommand<Void>()

    fun onSignInClick() {
        if (isInputDataValid(address.value, pinCode.value)) {
            appPreference.baseUrl = address.value
            val disposable = authInteractor.performAuthPinCode(pinCode.value)
                .observeOn(schedulersProvider.ui())
                .doOnSubscribe { isLoading.value = true }
                .doOnEvent { _, _ ->  isLoading.value = false}
                .subscribe({
                    pinCodeSuccessCommand.call()
                }, {
                    pinCodeErrorCommand.call()
                })
            unsubscribeOnDestroy(disposable)
        }
    }

    private fun isInputDataValid(address: String, pinCode: String): Boolean {
        addressValidCommand.call(address.isNotEmpty())
        pinCodeValidCommand.call(pinCode.isNotEmpty())
        return addressValidCommand.eventValue && pinCodeValidCommand.eventValue
    }
}