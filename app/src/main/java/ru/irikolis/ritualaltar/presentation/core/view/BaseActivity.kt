package ru.irikolis.ritualaltar.presentation.core.view

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.appcompat.app.AppCompatActivity
import androidx.annotation.LayoutRes
import android.os.Bundle
import ru.irikolis.ritualaltar.presentation.core.viewmodel.BaseViewModel

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity() {

    protected lateinit var binding: T

    protected abstract val model: V

    abstract val bindingVariable: Int

    @get:LayoutRes
    abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutId)
        binding.setVariable(bindingVariable, model)
        binding.executePendingBindings()
    }
}

