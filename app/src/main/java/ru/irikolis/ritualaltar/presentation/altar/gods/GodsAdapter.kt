package ru.irikolis.ritualaltar.presentation.altar.gods

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ru.irikolis.ritualaltar.databinding.ItemGodBinding
import ru.irikolis.ritualaltar.domain.models.God

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class GodsAdapter(
    private val model: GodsViewModel,
    var onItemClickListener: ((God) -> Unit)? = null
) : RecyclerView.Adapter<GodsAdapter.ViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<God>() {
        override fun areItemsTheSame(oldItem: God, newItem: God) = oldItem.godId == newItem.godId

        override fun areContentsTheSame(oldItem: God, newItem: God) = oldItem == newItem
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemGodBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ViewHolder(binding)
        binding.root.setOnClickListener {
            onItemClickListener?.let {
                val position = holder.adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    it.invoke(getItem(position))
                }
            }
        }
        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.model = model
        holder.binding.data = getItem(position)
    }

    override fun getItemCount(): Int = differ.currentList.size

    private fun getItem(position: Int) = differ.currentList[position]

    data class ViewHolder(val binding: ItemGodBinding) : RecyclerView.ViewHolder(binding.root)

    fun setGods(gods: List<God>) {
        differ.submitList(gods)
    }
}