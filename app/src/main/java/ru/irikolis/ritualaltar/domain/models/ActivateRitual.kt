package ru.irikolis.ritualaltar.domain.models

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class ActivateRitual(
    var result: String
)