package ru.irikolis.ritualaltar.domain.models

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
enum class RitualResultType {
    PREVIEW_RITUAL,
    ACTIVATION_RITUAL
}