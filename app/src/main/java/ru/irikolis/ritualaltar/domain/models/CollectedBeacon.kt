package ru.irikolis.ritualaltar.domain.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Parcelize
data class CollectedBeacon(
    var id: Int,
    var mac: String
) : Parcelable