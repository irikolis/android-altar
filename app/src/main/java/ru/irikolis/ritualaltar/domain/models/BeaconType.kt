package ru.irikolis.ritualaltar.domain.models

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
enum class BeaconType {
    ARTIFACT,
    SENIOR_PRIEST,
    PRIEST
}