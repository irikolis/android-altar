package ru.irikolis.ritualaltar.domain.interactors

import ru.irikolis.ritualaltar.data.repositories.AuthRepository
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class AuthInteractor(
    val authRepository: AuthRepository,
    val schedulersProvider: SchedulersProvider
) {
    fun performAuthLogin(username: String, password: String) = authRepository.performAuthLoginFromApi(username, password)
        .subscribeOn(schedulersProvider.io())

    fun performAuthPinCode(pinCode: String) = authRepository.performAuthPinCodeFromApi(pinCode)
        .subscribeOn(schedulersProvider.io())
}