package ru.irikolis.ritualaltar.domain.interactors

import io.reactivex.Observable
import io.reactivex.Single
import ru.irikolis.ritualaltar.data.providers.BluetoothProviderRepository
import ru.irikolis.ritualaltar.data.repositories.BeaconRepository
import ru.irikolis.ritualaltar.domain.models.Beacon
import ru.irikolis.ritualaltar.domain.models.CollectedBeacon
import ru.irikolis.ritualaltar.utils.SchedulersProvider
import java.lang.RuntimeException

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class BeaconInteractor(
    private val beaconRepository: BeaconRepository,
    private val bluetoothProviderRepository: BluetoothProviderRepository? = null,
    private val schedulersProvider: SchedulersProvider
) {
    fun getBeacons() = loadBeacons()
        .flatMap { getCollectedBeaconsFromDb(it) }
        .subscribeOn(schedulersProvider.io())

    fun detectBeacons() = loadBeacons()
        .flatMap { detectCollectedBeacons(it) }
        .subscribeOn(schedulersProvider.io())

    private fun loadBeacons() = beaconRepository.getBeaconsFromDb()
        .filter { it.isNotEmpty() }
        .switchIfEmpty(beaconRepository.getBeaconsFromApi()
            .flatMap { beacons -> beaconRepository.resetBeaconsDb(beacons).map { beacons } }
        )

    private fun detectCollectedBeacons(beacons: List<Beacon>): Single<MutableList<Beacon>> = bluetoothProviderRepository?.run {
        beaconRepository.clearCollectedBeaconsDb().toObservable()
            .flatMap { startBluetoothListener() }
            .flatMap { filterBeacons(beacons, it) }
            .flatMap { beacon -> beaconRepository.putCollectedBeaconDb(CollectedBeacon(beacon.id, beacon.mac)).map { beacon }.toObservable() }
            .toList()
    } ?: Single.error(RuntimeException())

    private fun getCollectedBeaconsFromDb(beacons: List<Beacon>) = beaconRepository.getCollectedBeaconsFromDb()
        .flattenAsObservable { items -> items }
        .flatMap { filterBeacons(beacons, it.mac) }
        .toList()

    private fun filterBeacons(beacons: List<Beacon>, mac: String) = Observable.just(beacons)
        .map { it.filter { beacon -> beacon.mac == mac } }
        .filter { it.isNotEmpty() }
        .map { it.first() }
}