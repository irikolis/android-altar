package ru.irikolis.ritualaltar.domain.models

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class God(
    var godId: Int,
    var name: String
)