package ru.irikolis.ritualaltar.domain.models

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class PreRitual(
    var result: String
)