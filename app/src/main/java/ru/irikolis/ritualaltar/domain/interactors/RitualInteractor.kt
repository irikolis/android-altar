package ru.irikolis.ritualaltar.domain.interactors

import ru.irikolis.ritualaltar.data.repositories.RitualRepository
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class RitualInteractor(
    private val ritualRepository: RitualRepository,
    private val schedulersProvider: SchedulersProvider
) {
    fun getRituals() = ritualRepository.getRitualsFromDb()
        .filter { it.isNotEmpty() }
        .switchIfEmpty(
            ritualRepository.getRitualsFromApi()
                .flatMap { rituals -> ritualRepository.resetRitualsDb(rituals).map { rituals } }
        )
        .subscribeOn(schedulersProvider.io())

    fun getRitual(ritualId: Int) = getRituals()
        .flattenAsObservable { items -> items }
        .filter { it.ritualId == ritualId }
        .firstOrError()
        .subscribeOn(schedulersProvider.io())

    fun performPreRitual(ritualId: Int, beaconIds: IntArray, quality: Int, dice: Int) =
        ritualRepository.performPreRitualFromApi(
            ritualId,
            beaconIds,
            quality,
            dice
        )
            .flatMap { preview -> ritualRepository.putResultToDb(preview.result).map { preview } }
            .subscribeOn(schedulersProvider.io())

    fun performActivateRitual(ritualId: Int, beaconIds: IntArray, quality: Int, dice: Int) =
        ritualRepository.performActivateRitualFromApi(
            ritualId,
            beaconIds,
            quality,
            dice
        )
            .flatMap { preview -> ritualRepository.putResultToDb(preview.result).map { preview } }
            .subscribeOn(schedulersProvider.io())

    fun getRitualResult() = ritualRepository.getRitualResultFromDb()
        .subscribeOn(schedulersProvider.io())
}