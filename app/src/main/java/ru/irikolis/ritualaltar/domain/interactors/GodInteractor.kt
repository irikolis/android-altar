package ru.irikolis.ritualaltar.domain.interactors

import ru.irikolis.ritualaltar.data.repositories.GodRepository
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class GodInteractor(
    private val godRepository: GodRepository,
    private val schedulersProvider: SchedulersProvider
) {
    fun getGods() = godRepository.getGodsFromDb()
        .filter { it.isNotEmpty() }
        .switchIfEmpty(
            godRepository.getGodsFromApi()
                .flatMap { gods -> godRepository.resetGodsDb(gods).map { gods } }
        )
        .subscribeOn(schedulersProvider.io())
}