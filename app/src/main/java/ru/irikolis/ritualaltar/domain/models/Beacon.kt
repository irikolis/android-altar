package ru.irikolis.ritualaltar.domain.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Parcelize
data class Beacon(
    var id: Int,
    var mac: String,
    var name: String,
    var description: String,
    var force: Int,
    var type: BeaconType
) : Parcelable