package ru.irikolis.ritualaltar.domain.models

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
data class Ritual(
    var ritualId: Int,
    var godId: Int,
    var name: String,
    var priestCnt: Int,
    var seniorPriestCnt: Int,
    var resources: String,
    var effect: String
)