package ru.irikolis.ritualaltar.di.component

import dagger.Subcomponent
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.di.module.ResultRitualModule
import ru.irikolis.ritualaltar.presentation.altar.resultritual.ResultRitualFragment

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@FragmentScope
@Subcomponent(
    modules = [
        ResultRitualModule::class
    ]
)
interface ResultRitualComponent {
    fun inject(fragment: ResultRitualFragment)
}