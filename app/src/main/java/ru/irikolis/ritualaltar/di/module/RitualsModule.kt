package ru.irikolis.ritualaltar.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.irikolis.ritualaltar.data.database.AppDatabase
import ru.irikolis.ritualaltar.data.network.ApiService
import ru.irikolis.ritualaltar.data.repositories.BeaconRepository
import ru.irikolis.ritualaltar.data.repositories.RitualRepository
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.presentation.altar.rituals.RitualsAdapter
import ru.irikolis.ritualaltar.presentation.altar.rituals.RitualsFragment
import ru.irikolis.ritualaltar.presentation.altar.rituals.RitualsViewModel
import ru.irikolis.ritualaltar.di.factory.RitualsViewModelFactory
import ru.irikolis.ritualaltar.domain.interactors.BeaconInteractor
import ru.irikolis.ritualaltar.domain.interactors.RitualInteractor
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
class RitualsModule(private val fragment: RitualsFragment) {
    @Provides
    @FragmentScope
    fun provideRitualsViewModel(
        factory: RitualsViewModelFactory
    ): RitualsViewModel =
        ViewModelProvider(fragment, factory).get(RitualsViewModel::class.java)

    @Provides
    @FragmentScope
    fun provideRitualsAdapter(
        model: RitualsViewModel
    ): RitualsAdapter =
        RitualsAdapter(model)

    @Provides
    @FragmentScope
    fun provideRitualInteractor(
        ritualRepository: RitualRepository,
        schedulersProvider: SchedulersProvider
    ): RitualInteractor =
        RitualInteractor(ritualRepository, schedulersProvider)

    @Provides
    @FragmentScope
    fun provideBeaconInteractor(
        beaconRepository: BeaconRepository,
        schedulersProvider: SchedulersProvider
    ): BeaconInteractor =
        BeaconInteractor(beaconRepository, schedulersProvider = schedulersProvider)

    @Provides
    @FragmentScope
    fun provideRitualRepository(
        apiService: ApiService,
        appDatabase: AppDatabase
    ): RitualRepository =
        RitualRepository(apiService, appDatabase)

    @Provides
    @FragmentScope
    fun provideBeaconRepository(
        apiService: ApiService,
        appDatabase: AppDatabase
    ): BeaconRepository =
        BeaconRepository(apiService, appDatabase)
}