package ru.irikolis.ritualaltar.di.component

import dagger.Subcomponent
import ru.irikolis.ritualaltar.di.ActivityScope
import ru.irikolis.ritualaltar.di.module.*
import ru.irikolis.ritualaltar.presentation.altar.AltarActivity

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@ActivityScope
@Subcomponent(
    modules = [
        AltarModule::class,
        NavigationModule::class
    ]
)
interface AltarComponent {
    fun inject(altarActivity: AltarActivity)

    fun plusGodsComponent(godsModule: GodsModule): GodsComponent
    fun plusBeaconsComponent(beaconsModule: BeaconsModule): BeaconsComponent
    fun plusRitualsComponent(ritualsModule: RitualsModule): RitualsComponent
    fun plusPreRitualComponent(preRitualModule: PreRitualModule): PreRitualComponent
    fun plusResultRitualComponent(resultRitualModule: ResultRitualModule): ResultRitualComponent
}