package ru.irikolis.ritualaltar.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import ru.irikolis.ritualaltar.RitualAltarApplication
import ru.irikolis.ritualaltar.di.module.*
import javax.inject.Singleton

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class
    ]
)
interface AppComponent {
    fun inject(application: RitualAltarApplication)

    @Component.Builder
    interface Builder {
        fun build(): AppComponent
        @BindsInstance
        fun application(application: Application): Builder
    }

    fun plusMainComponent(mainModule: MainModule): MainComponent
    fun plusAuthComponent(authModule: AuthModule): AuthComponent
    fun plusAltarComponent(altarModule: AltarModule): AltarComponent
}