package ru.irikolis.ritualaltar.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.irikolis.ritualaltar.data.database.AppDatabase
import ru.irikolis.ritualaltar.data.network.ApiService
import ru.irikolis.ritualaltar.data.repositories.BeaconRepository
import ru.irikolis.ritualaltar.data.repositories.RitualRepository
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.di.factory.PreRitualViewModelFactory
import ru.irikolis.ritualaltar.domain.interactors.BeaconInteractor
import ru.irikolis.ritualaltar.domain.interactors.RitualInteractor
import ru.irikolis.ritualaltar.presentation.altar.preritual.PreRitualFragment
import ru.irikolis.ritualaltar.presentation.altar.preritual.PreRitualViewModel
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
class PreRitualModule(private val fragment: PreRitualFragment) {

    @Provides
    @FragmentScope
    fun providePreRitualViewModel(
        factory: PreRitualViewModelFactory
    ): PreRitualViewModel =
        ViewModelProvider(fragment, factory).get(PreRitualViewModel::class.java)

    @Provides
    @FragmentScope
    fun provideRitualInteractor(
        ritualRepository: RitualRepository,
        schedulersProvider: SchedulersProvider
    ): RitualInteractor =
        RitualInteractor(ritualRepository, schedulersProvider)

    @Provides
    @FragmentScope
    fun provideBeaconInteractor(
        beaconRepository: BeaconRepository,
        schedulersProvider: SchedulersProvider
    ): BeaconInteractor =
        BeaconInteractor(beaconRepository, schedulersProvider = schedulersProvider)

    @Provides
    @FragmentScope
    fun provideRitualRepository(
        apiService: ApiService,
        appDatabase: AppDatabase
    ): RitualRepository =
        RitualRepository(apiService, appDatabase)

    @Provides
    @FragmentScope
    fun provideBeaconRepository(
        apiService: ApiService,
        appDatabase: AppDatabase
    ): BeaconRepository =
        BeaconRepository(apiService, appDatabase)
}