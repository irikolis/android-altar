package ru.irikolis.ritualaltar.di.component

import dagger.Subcomponent
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.di.module.RitualsModule
import ru.irikolis.ritualaltar.presentation.altar.rituals.RitualsFragment

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@FragmentScope
@Subcomponent(
    modules = [
        RitualsModule::class
    ]
)
interface RitualsComponent {
    fun inject(fragment: RitualsFragment)
}