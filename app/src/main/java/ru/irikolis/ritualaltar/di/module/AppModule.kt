package ru.irikolis.ritualaltar.di.module

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.irikolis.ritualaltar.AppConstants
import ru.irikolis.ritualaltar.data.database.AppDatabase
import ru.irikolis.ritualaltar.data.network.ApiService
import ru.irikolis.ritualaltar.data.network.OkHttpProvider
import ru.irikolis.ritualaltar.data.preference.AppPreference
import ru.irikolis.ritualaltar.utils.SchedulersProvider
import javax.inject.Singleton

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideAppPreference(
        application: Application
    ): AppPreference =
        AppPreference(application, AppConstants.APP_PREFERENCES)

    @Provides
    @Singleton
    fun provideRetrofit(
        appPreference: AppPreference
    ): Retrofit =
        Retrofit.Builder()
            .baseUrl(String.format("http://%s/", AppConstants.BASE_URL))
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(OkHttpProvider.buildClient(appPreference))
            .build()

    @Provides
    @Singleton
    fun provideApiService(
        retrofit: Retrofit
    ): ApiService =
        retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideAppDatabase(
        application: Application
    ): AppDatabase =
        Room.databaseBuilder(application, AppDatabase::class.java, AppConstants.APP_DATABASE)
            .build()

    @Provides
    @Singleton
    fun provideSchedulersProvider(): SchedulersProvider {
        return SchedulersProvider()
    }
}