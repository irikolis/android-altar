package ru.irikolis.ritualaltar.di.component

import dagger.Subcomponent
import ru.irikolis.ritualaltar.di.ActivityScope
import ru.irikolis.ritualaltar.di.module.*
import ru.irikolis.ritualaltar.presentation.main.MainActivity

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@ActivityScope
@Subcomponent(
    modules = [
        MainModule::class
    ]
)
interface MainComponent {
    fun inject(mainActivity: MainActivity)
}