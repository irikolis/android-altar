package ru.irikolis.ritualaltar.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.irikolis.ritualaltar.di.ActivityScope
import ru.irikolis.ritualaltar.presentation.main.MainActivity
import ru.irikolis.ritualaltar.presentation.main.MainViewModel

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
class MainModule(private val activity: MainActivity) {

    @Provides
    @ActivityScope
    fun provideMainViewModel(): MainViewModel =
        ViewModelProvider(activity).get(MainViewModel::class.java)
}