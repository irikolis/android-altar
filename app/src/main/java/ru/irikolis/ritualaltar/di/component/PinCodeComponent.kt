package ru.irikolis.ritualaltar.di.component

import dagger.Subcomponent
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.di.module.PinCodeModule
import ru.irikolis.ritualaltar.presentation.authorization.pincode.PinCodeFragment

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@FragmentScope
@Subcomponent(
    modules = [
        PinCodeModule::class
    ]
)
interface PinCodeComponent {
    fun inject(fragment: PinCodeFragment)
}