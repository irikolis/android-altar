package ru.irikolis.ritualaltar.di.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.irikolis.ritualaltar.domain.interactors.BeaconInteractor
import ru.irikolis.ritualaltar.presentation.altar.beacons.BeaconsViewModel
import ru.irikolis.ritualaltar.utils.SchedulersProvider
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class BeaconsViewModelFactory @Inject constructor(
    private val beaconInteractor: BeaconInteractor,
    private val schedulersProvider: SchedulersProvider
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        require(modelClass == BeaconsViewModel::class.java) { "Unknown ViewModel class" }
        return BeaconsViewModel(beaconInteractor, schedulersProvider) as T
    }
}