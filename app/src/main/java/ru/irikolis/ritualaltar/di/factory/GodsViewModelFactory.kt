package ru.irikolis.ritualaltar.di.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.irikolis.ritualaltar.data.preference.AppPreference
import ru.irikolis.ritualaltar.domain.interactors.GodInteractor
import ru.irikolis.ritualaltar.presentation.altar.gods.GodsViewModel
import ru.irikolis.ritualaltar.utils.SchedulersProvider
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class GodsViewModelFactory @Inject constructor(
    private val appPreference: AppPreference,
    private val godInteractor: GodInteractor,
    private val schedulersProvider: SchedulersProvider
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        require(modelClass == GodsViewModel::class.java) { "Unknown ViewModel class" }
        return GodsViewModel(
            appPreference,
            godInteractor,
            schedulersProvider
        ) as T
    }
}