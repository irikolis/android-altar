package ru.irikolis.ritualaltar.di.component

import dagger.Subcomponent
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.di.module.BeaconsModule
import ru.irikolis.ritualaltar.presentation.altar.beacons.BeaconsFragment

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@FragmentScope
@Subcomponent(
    modules = [
        BeaconsModule::class
    ]
)
interface BeaconsComponent {
    fun inject(fragment: BeaconsFragment)
}