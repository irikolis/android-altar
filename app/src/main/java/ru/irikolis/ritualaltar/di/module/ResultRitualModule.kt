package ru.irikolis.ritualaltar.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.irikolis.ritualaltar.data.database.AppDatabase
import ru.irikolis.ritualaltar.data.network.ApiService
import ru.irikolis.ritualaltar.data.repositories.RitualRepository
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.di.factory.ResultRitualViewModelFactory
import ru.irikolis.ritualaltar.domain.interactors.RitualInteractor
import ru.irikolis.ritualaltar.presentation.altar.resultritual.ResultRitualFragment
import ru.irikolis.ritualaltar.presentation.altar.resultritual.ResultRitualViewModel
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
class ResultRitualModule(private val fragment: ResultRitualFragment) {

    @Provides
    @FragmentScope
    fun provideResultRitualViewModel(
        factory: ResultRitualViewModelFactory
    ): ResultRitualViewModel =
        ViewModelProvider(fragment, factory).get(ResultRitualViewModel::class.java)

    @Provides
    @FragmentScope
    fun provideRitualInteractor(
        ritualRepository: RitualRepository,
        schedulersProvider: SchedulersProvider
    ): RitualInteractor =
        RitualInteractor(ritualRepository, schedulersProvider)

    @Provides
    @FragmentScope
    fun provideRitualRepository(
        apiService: ApiService,
        appDatabase: AppDatabase
    ): RitualRepository =
        RitualRepository(apiService, appDatabase)
}