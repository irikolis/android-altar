package ru.irikolis.ritualaltar.di.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.irikolis.ritualaltar.domain.interactors.RitualInteractor
import ru.irikolis.ritualaltar.presentation.altar.resultritual.ResultRitualViewModel
import ru.irikolis.ritualaltar.utils.SchedulersProvider
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class ResultRitualViewModelFactory @Inject constructor(
    private val ritualInteractor: RitualInteractor,
    private val schedulersProvider: SchedulersProvider
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        require(modelClass == ResultRitualViewModel::class.java) { "Unknown ViewModel class" }
        return ResultRitualViewModel(
            ritualInteractor,
            schedulersProvider
        ) as T
    }
}