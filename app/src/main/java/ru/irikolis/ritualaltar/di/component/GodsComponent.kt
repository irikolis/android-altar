package ru.irikolis.ritualaltar.di.component

import dagger.Subcomponent
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.di.module.GodsModule
import ru.irikolis.ritualaltar.presentation.altar.gods.GodsFragment

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@FragmentScope
@Subcomponent(
    modules = [
        GodsModule::class
    ]
)
interface GodsComponent {
    fun inject(fragment: GodsFragment)
}