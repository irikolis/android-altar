package ru.irikolis.ritualaltar.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.irikolis.ritualaltar.data.database.AppDatabase
import ru.irikolis.ritualaltar.data.network.ApiService
import ru.irikolis.ritualaltar.data.preference.AppPreference
import ru.irikolis.ritualaltar.data.repositories.AuthRepository
import ru.irikolis.ritualaltar.di.ActivityScope
import ru.irikolis.ritualaltar.domain.interactors.AuthInteractor
import ru.irikolis.ritualaltar.presentation.authorization.AuthActivity
import ru.irikolis.ritualaltar.presentation.authorization.AuthViewModel
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
class AuthModule(private val activity: AuthActivity) {

    @Provides
    @ActivityScope
    fun provideAuthViewModel(): AuthViewModel =
        ViewModelProvider(activity).get(AuthViewModel::class.java)

    @Provides
    @ActivityScope
    fun provideAuthInteractor(
        authRepository: AuthRepository,
        schedulersProvider: SchedulersProvider
    ): AuthInteractor =
        AuthInteractor(authRepository, schedulersProvider)

    @Provides
    @ActivityScope
    fun provideAuthRepository(
        apiService: ApiService,
        appPreference: AppPreference,
        appDatabase: AppDatabase
    ): AuthRepository =
        AuthRepository(apiService, appPreference, appDatabase)
}