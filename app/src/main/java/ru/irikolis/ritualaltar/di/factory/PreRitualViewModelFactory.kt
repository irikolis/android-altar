package ru.irikolis.ritualaltar.di.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.irikolis.ritualaltar.data.preference.AppPreference
import ru.irikolis.ritualaltar.domain.interactors.BeaconInteractor
import ru.irikolis.ritualaltar.domain.interactors.RitualInteractor
import ru.irikolis.ritualaltar.presentation.altar.preritual.PreRitualViewModel
import ru.irikolis.ritualaltar.utils.SchedulersProvider
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class PreRitualViewModelFactory @Inject constructor(
    private val appPreference: AppPreference,
    private val ritualInteractor: RitualInteractor,
    private val beaconInteractor: BeaconInteractor,
    private val schedulersProvider: SchedulersProvider
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        require(modelClass == PreRitualViewModel::class.java) { "Unknown ViewModel class" }
        return PreRitualViewModel(
            appPreference,
            ritualInteractor,
            beaconInteractor,
            schedulersProvider
        ) as T
    }
}