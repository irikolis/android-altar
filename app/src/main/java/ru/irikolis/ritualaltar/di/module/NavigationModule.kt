package ru.irikolis.ritualaltar.di.module

import dagger.Module
import dagger.Provides
import ru.irikolis.ritualaltar.di.ActivityScope
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
class NavigationModule {

    @Provides
    @ActivityScope
    fun getCicerone(): Cicerone<Router> {
        return Cicerone.create()
    }

    @Provides
    @ActivityScope
    fun getRouter(cicerone: Cicerone<Router>): Router {
        return cicerone.router
    }

    @Provides
    @ActivityScope
    fun getNavigationHolder(cicerone: Cicerone<Router>): NavigatorHolder {
        return cicerone.navigatorHolder
    }
}