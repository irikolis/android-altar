package ru.irikolis.ritualaltar.di.component

import dagger.Subcomponent
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.di.module.LoginModule
import ru.irikolis.ritualaltar.presentation.authorization.login.LoginFragment

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@FragmentScope
@Subcomponent(
    modules = [
        LoginModule::class
    ]
)
interface LoginComponent {
    fun inject(fragment: LoginFragment)
}