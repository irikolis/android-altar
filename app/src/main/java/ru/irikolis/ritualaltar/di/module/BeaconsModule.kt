package ru.irikolis.ritualaltar.di.module

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.irikolis.ritualaltar.data.database.AppDatabase
import ru.irikolis.ritualaltar.data.network.ApiService
import ru.irikolis.ritualaltar.data.providers.BluetoothProviderRepository
import ru.irikolis.ritualaltar.data.repositories.BeaconRepository
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.presentation.altar.beacons.BeaconsAdapter
import ru.irikolis.ritualaltar.presentation.altar.beacons.BeaconsFragment
import ru.irikolis.ritualaltar.presentation.altar.beacons.BeaconsViewModel
import ru.irikolis.ritualaltar.di.factory.BeaconsViewModelFactory
import ru.irikolis.ritualaltar.domain.interactors.BeaconInteractor
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
class BeaconsModule(private val fragment: BeaconsFragment) {

    @Provides
    @FragmentScope
    fun provideBeaconsViewModel(
        factory: BeaconsViewModelFactory
    ): BeaconsViewModel =
        ViewModelProvider(fragment, factory).get(BeaconsViewModel::class.java)

    @Provides
    @FragmentScope
    fun provideBeaconsAdapter(
        model: BeaconsViewModel
    ): BeaconsAdapter =
        BeaconsAdapter(model)

    @Provides
    @FragmentScope
    fun provideBeaconInteractor(
        beaconRepository: BeaconRepository,
        bluetoothProviderRepository: BluetoothProviderRepository,
        schedulersProvider: SchedulersProvider
    ): BeaconInteractor =
        BeaconInteractor(beaconRepository, bluetoothProviderRepository, schedulersProvider)

    @Provides
    @FragmentScope
    fun provideBeaconRepository(
        apiService: ApiService,
        appDatabase: AppDatabase
    ): BeaconRepository =
        BeaconRepository(apiService, appDatabase)

    @Provides
    @FragmentScope
    fun provideBluetoothProviderRepository(
        application: Application
    ): BluetoothProviderRepository =
        BluetoothProviderRepository(application)
}