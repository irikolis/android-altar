package ru.irikolis.ritualaltar.di

import javax.inject.Scope

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class FragmentScope