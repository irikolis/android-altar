package ru.irikolis.ritualaltar.di

import android.app.Application
import ru.irikolis.ritualaltar.di.component.*
import ru.irikolis.ritualaltar.di.module.*
import ru.irikolis.ritualaltar.presentation.altar.AltarActivity
import ru.irikolis.ritualaltar.presentation.altar.beacons.BeaconsFragment
import ru.irikolis.ritualaltar.presentation.altar.gods.GodsFragment
import ru.irikolis.ritualaltar.presentation.altar.preritual.PreRitualFragment
import ru.irikolis.ritualaltar.presentation.altar.resultritual.ResultRitualFragment
import ru.irikolis.ritualaltar.presentation.altar.rituals.RitualsFragment
import ru.irikolis.ritualaltar.presentation.authorization.AuthActivity
import ru.irikolis.ritualaltar.presentation.authorization.login.LoginFragment
import ru.irikolis.ritualaltar.presentation.authorization.pincode.PinCodeFragment
import ru.irikolis.ritualaltar.presentation.main.MainActivity

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class ComponentHolder {
    private var appComponent: AppComponent? = null
    private var mainComponent: MainComponent? = null
    private var authComponent: AuthComponent? = null
    private var loginComponent: LoginComponent? = null
    private var pinCodeComponent: PinCodeComponent? = null
    private var altarComponent: AltarComponent? = null
    private var godsComponent: GodsComponent? = null
    private var beaconsComponent: BeaconsComponent? = null
    private var ritualsComponent: RitualsComponent? = null
    private var preRitualComponent: PreRitualComponent? = null
    private var resultRitualComponent: ResultRitualComponent? = null

    companion object {
        fun create(application: Application): ComponentHolder {
            val factory = ComponentHolder()
            factory.appComponent = DaggerAppComponent.builder()
                .application(application)
                .build()
            return factory
        }
    }


    fun getMainComponent(activity: MainActivity): MainComponent {
        mainComponent = mainComponent ?: appComponent?.plusMainComponent(MainModule(activity))
        return mainComponent!!
    }

    fun clearMainComponent() {
        mainComponent = null
    }

    fun getAuthComponent(activity: AuthActivity): AuthComponent {
        authComponent = authComponent ?: appComponent?.plusAuthComponent(AuthModule(activity))
        return authComponent!!
    }

    fun clearAuthComponent() {
        authComponent = null
    }

    fun getLoginComponent(fragment: LoginFragment): LoginComponent {
        loginComponent = loginComponent ?: authComponent?.plusLoginComponent(LoginModule(fragment))
        return loginComponent!!
    }

    fun clearLoginComponent() {
        loginComponent = null
    }

    fun getPinCodeComponent(fragment: PinCodeFragment): PinCodeComponent {
        pinCodeComponent = pinCodeComponent ?: authComponent?.plusPinCodeComponent(PinCodeModule(fragment))
        return pinCodeComponent!!
    }

    fun clearPinCodeComponent() {
        pinCodeComponent = null
    }

    fun getAltarComponent(activity: AltarActivity): AltarComponent {
        altarComponent = altarComponent ?: appComponent?.plusAltarComponent(AltarModule(activity))
        return altarComponent!!
    }

    fun clearAltarComponent() {
        altarComponent = null
    }

    fun getGodsComponent(fragment: GodsFragment): GodsComponent {
        godsComponent = godsComponent ?: altarComponent?.plusGodsComponent(GodsModule(fragment))
        return godsComponent!!
    }

    fun clearGodsComponent() {
        godsComponent = null
    }

    fun getBeaconsComponent(fragment: BeaconsFragment): BeaconsComponent {
        beaconsComponent = beaconsComponent ?: altarComponent?.plusBeaconsComponent(BeaconsModule(fragment))
        return beaconsComponent!!
    }

    fun clearBeaconsComponent() {
        beaconsComponent = null
    }

    fun getRitualsComponent(fragment: RitualsFragment): RitualsComponent {
        ritualsComponent = ritualsComponent ?: altarComponent?.plusRitualsComponent(RitualsModule(fragment))
        return ritualsComponent!!
    }

    fun clearRitualsComponent() {
        ritualsComponent = null
    }

    fun getPreRitualComponent(fragment: PreRitualFragment): PreRitualComponent {
        preRitualComponent = preRitualComponent ?: altarComponent?.plusPreRitualComponent(
            PreRitualModule(fragment)
        )
        return preRitualComponent!!
    }

    fun clearPreRitualComponent() {
        preRitualComponent = null
    }

    fun getResultRitualComponent(fragment: ResultRitualFragment): ResultRitualComponent {
        resultRitualComponent = resultRitualComponent ?: altarComponent?.plusResultRitualComponent(
            ResultRitualModule(fragment)
        )
        return resultRitualComponent!!
    }

    fun clearResultRitualComponent() {
        resultRitualComponent = null
    }
}