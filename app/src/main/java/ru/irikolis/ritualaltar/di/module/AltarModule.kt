package ru.irikolis.ritualaltar.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.irikolis.ritualaltar.di.ActivityScope
import ru.irikolis.ritualaltar.presentation.altar.AltarActivity
import ru.irikolis.ritualaltar.presentation.altar.AltarViewModel

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
class AltarModule(private val activity: AltarActivity) {

    @Provides
    @ActivityScope
    fun provideAltarViewModel(): AltarViewModel =
        ViewModelProvider(activity).get(AltarViewModel::class.java)
}