package ru.irikolis.ritualaltar.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.irikolis.ritualaltar.data.database.AppDatabase
import ru.irikolis.ritualaltar.data.network.ApiService
import ru.irikolis.ritualaltar.data.repositories.GodRepository
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.presentation.altar.gods.GodsAdapter
import ru.irikolis.ritualaltar.presentation.altar.gods.GodsFragment
import ru.irikolis.ritualaltar.presentation.altar.gods.GodsViewModel
import ru.irikolis.ritualaltar.di.factory.GodsViewModelFactory
import ru.irikolis.ritualaltar.domain.interactors.GodInteractor
import ru.irikolis.ritualaltar.utils.SchedulersProvider

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
class GodsModule(private val fragment: GodsFragment) {

    @Provides
    @FragmentScope
    fun provideGodsViewModel(
        factory: GodsViewModelFactory
    ): GodsViewModel =
        ViewModelProvider(fragment, factory).get(GodsViewModel::class.java)

    @Provides
    @FragmentScope
    fun provideGodsAdapter(
        model: GodsViewModel
    ): GodsAdapter =
        GodsAdapter(model)

    @Provides
    @FragmentScope
    fun provideGodInteractor(
        godRepository: GodRepository,
        schedulersProvider: SchedulersProvider
    ): GodInteractor =
        GodInteractor(godRepository, schedulersProvider)

    @Provides
    @FragmentScope
    fun provideGodRepository(
        apiService: ApiService,
        appDatabase: AppDatabase
    ): GodRepository =
        GodRepository(apiService, appDatabase)
}