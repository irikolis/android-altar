package ru.irikolis.ritualaltar.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.presentation.authorization.login.LoginFragment
import ru.irikolis.ritualaltar.presentation.authorization.login.LoginViewModel
import ru.irikolis.ritualaltar.di.factory.LoginViewModelFactory

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
class LoginModule(private val fragment: LoginFragment) {

    @Provides
    @FragmentScope
    fun provideLoginViewModel(
        factory: LoginViewModelFactory
    ): LoginViewModel =
        ViewModelProvider(fragment, factory).get(LoginViewModel::class.java)

}