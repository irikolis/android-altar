package ru.irikolis.ritualaltar.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.di.factory.PinCodeViewModelFactory
import ru.irikolis.ritualaltar.presentation.authorization.pincode.PinCodeFragment
import ru.irikolis.ritualaltar.presentation.authorization.pincode.PinCodeViewModel

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@Module
class PinCodeModule(private val fragment: PinCodeFragment) {

    @Provides
    @FragmentScope
    fun providePinCodeViewModel(
        factory: PinCodeViewModelFactory
    ): PinCodeViewModel =
        ViewModelProvider(fragment, factory).get(PinCodeViewModel::class.java)
}