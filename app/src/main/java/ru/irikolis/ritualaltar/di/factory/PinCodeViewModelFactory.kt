package ru.irikolis.ritualaltar.di.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.irikolis.ritualaltar.data.preference.AppPreference
import ru.irikolis.ritualaltar.domain.interactors.AuthInteractor
import ru.irikolis.ritualaltar.presentation.authorization.pincode.PinCodeViewModel
import ru.irikolis.ritualaltar.utils.SchedulersProvider
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class PinCodeViewModelFactory @Inject constructor(
    val appPreference: AppPreference,
    val authInteractor: AuthInteractor,
    val schedulersProvider: SchedulersProvider
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        require(modelClass == PinCodeViewModel::class.java) { "Unknown ViewModel class" }
        return PinCodeViewModel(appPreference, authInteractor, schedulersProvider) as T
    }
}