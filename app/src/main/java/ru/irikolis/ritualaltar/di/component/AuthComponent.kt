package ru.irikolis.ritualaltar.di.component

import dagger.Subcomponent
import ru.irikolis.ritualaltar.di.ActivityScope
import ru.irikolis.ritualaltar.di.module.AuthModule
import ru.irikolis.ritualaltar.di.module.LoginModule
import ru.irikolis.ritualaltar.di.module.PinCodeModule
import ru.irikolis.ritualaltar.presentation.authorization.AuthActivity

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@ActivityScope
@Subcomponent(
    modules = [
        AuthModule::class
    ]
)
interface AuthComponent {
    fun inject(authActivity: AuthActivity)

    fun plusLoginComponent(loginModule: LoginModule): LoginComponent
    fun plusPinCodeComponent(pinCodeModule: PinCodeModule): PinCodeComponent
}