package ru.irikolis.ritualaltar.di.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.irikolis.ritualaltar.data.preference.AppPreference
import ru.irikolis.ritualaltar.domain.interactors.AuthInteractor
import ru.irikolis.ritualaltar.presentation.authorization.login.LoginViewModel
import ru.irikolis.ritualaltar.utils.SchedulersProvider
import javax.inject.Inject

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class LoginViewModelFactory @Inject constructor(
    private val appPreference: AppPreference,
    private val authInteractor: AuthInteractor,
    private val schedulersProvider: SchedulersProvider
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        require(modelClass == LoginViewModel::class.java) { "Unknown ViewModel class" }
        return LoginViewModel(appPreference, authInteractor, schedulersProvider) as T
    }
}