package ru.irikolis.ritualaltar.di.component

import dagger.Subcomponent
import ru.irikolis.ritualaltar.di.FragmentScope
import ru.irikolis.ritualaltar.di.module.PreRitualModule
import ru.irikolis.ritualaltar.presentation.altar.preritual.PreRitualFragment

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
@FragmentScope
@Subcomponent(
    modules = [
        PreRitualModule::class
    ]
)
interface PreRitualComponent {
    fun inject(fragment: PreRitualFragment)
}