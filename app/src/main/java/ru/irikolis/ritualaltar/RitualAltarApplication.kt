package ru.irikolis.ritualaltar

import android.app.Application
import ru.irikolis.ritualaltar.di.ComponentHolder
import timber.log.Timber
import java.lang.RuntimeException

/**
 * @author Irina Kolovorotnaya (irikolis)
 */
class RitualAltarApplication : Application() {
    companion object {
        lateinit var application: RitualAltarApplication
            private set
    }


    private var holder: ComponentHolder? = null

    val components: ComponentHolder
        get() = holder ?: throw RuntimeException("Field 'components' is null")

    override fun onCreate() {
        super.onCreate()
        application = this

        initDagger()

        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }

    private fun initDagger() {
        holder = ComponentHolder.create(this)
    }
}