package ru.irikolis.ritualaltar.data.providers

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import io.reactivex.Observable
import ru.irikolis.ritualaltar.utils.createBroadcastReceiver

class BluetoothProviderRepository(
    private val context: Context
) {

    fun isBluetoothEnabled(): Boolean {
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter() ?: return false
        return if (!bluetoothAdapter.isEnabled) {
            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            false
        } else true
    }

    fun startBluetoothListener(): Observable<Long?> {
        val intentFilter = IntentFilter()
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND)
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        return createBroadcastReceiver(context, intentFilter)
            .flatMap { intent ->
                intent.action?.let { action ->
                    if (action == BluetoothDevice.ACTION_FOUND) {
                        val device: BluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                        device?.let {
                           if (it.bondState == BluetoothDevice.BOND_BONDED) {
                               return@flatMap Observable.just(it.address)
                           }
                        }
                    }
                }
                Observable.empty()
            }
            .map { macString -> macString.replace(":", "").toLong(16) }
    }
}