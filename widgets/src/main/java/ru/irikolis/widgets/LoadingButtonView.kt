package ru.irikolis.widgets

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ProgressBar

/**
 * @author Irina Kolovorotnaya (irikolis)
 */

private const val DEFAULT_BACKGROUND_COLOR = Color.WHITE
private const val DEFAULT_TEXT_COLOR = Color.BLACK
private const val DEFAULT_DURATION = 300

@Suppress("unused", "MemberVisibilityCanBePrivate")
class LoadingButtonView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0,
        defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyleAttr, defStyleRes) {
    var button: Button
        private set

    var progress: ProgressBar
        private set

    var originBackground: Drawable = ColorDrawable(DEFAULT_BACKGROUND_COLOR)
    var morphBackground: Drawable = ColorDrawable(DEFAULT_BACKGROUND_COLOR)

    var text: CharSequence
        get() = button.text
        set(value) { button.text = value }

    var textColor: Int
        get() = button.currentTextColor
        set(value) { button.setTextColor(value) }

    var loading: Boolean = false
        private set

    var animateDuration: Long = DEFAULT_DURATION.toLong()

    override fun setOnClickListener(l: OnClickListener?) = button.setOnClickListener(l)

    private var originWidth: Int = 0
    private var morphWidth: Int = 0

    private var originHeight: Int = 0
    private var morphHeight: Int = 0

    private var animator: ValueAnimator? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.view_loading_button, this, true)
        button = findViewById(R.id.button_load)
        progress = findViewById(R.id.progress_loading)

        attrs?.let {
            val a = context.obtainStyledAttributes(
                    attrs,
                    R.styleable.LoadingButtonView,
                    defStyleAttr,
                    defStyleRes
            )

            originBackground = a.getDrawable(R.styleable.LoadingButtonView_lb_originBackground)
                    ?: ColorDrawable(DEFAULT_BACKGROUND_COLOR)

            morphBackground = a.getDrawable(R.styleable.LoadingButtonView_lb_morphBackground)
                    ?: ColorDrawable(DEFAULT_BACKGROUND_COLOR)

            text = a.getText(R.styleable.LoadingButtonView_lb_text)

            textColor = a.getColor(R.styleable.LoadingButtonView_lb_textColor, DEFAULT_TEXT_COLOR)

            animateDuration = a.getInteger(R.styleable.LoadingButtonView_lb_animateDuration, DEFAULT_DURATION).toLong()

            a.recycle()
        }
        background = if (loading) morphBackground else originBackground

        viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                originWidth = width
                morphWidth = height
                originHeight = height
                morphHeight = height
                setLoading(loading)
            }
        })
    }

    internal class SavedState : BaseSavedState {
        var loading: Boolean = false

        constructor(state: Parcelable?) : super(state)

        constructor(source: Parcel) : super(source) {
            loading = (source.readInt() == 1)
        }

        companion object {
            @JvmField
            val CREATOR = object : Parcelable.Creator<SavedState> {
                override fun createFromParcel(source: Parcel) = SavedState(source)
                override fun newArray(size: Int) = arrayOfNulls<SavedState>(size)
            }
        }

        override fun writeToParcel(dest: Parcel?, flags: Int) {
            dest?.writeInt(if (loading) 1 else 0)
        }

        override fun describeContents() = 0
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        (state as? SavedState)?.let {
            loading = it.loading
        }
        super.onRestoreInstanceState(state)
    }

    override fun onSaveInstanceState(): Parcelable {
        val state = SavedState(super.onSaveInstanceState())
        state.loading = this.loading
        return state
    }

    override fun setVisibility(visibility: Int) {
        super.setVisibility(visibility)
        if (visibility == View.VISIBLE) {
            originWidth = width
            morphWidth = height
            originHeight = height
            morphHeight = height
        }
    }

    private fun getWrapWidth(view: View): Int {
        val params = view.layoutParams

        val width = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
        val height = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
        view.measure(width, height)
        val measureWidth = view.measuredWidth

        view.layoutParams = params
        return measureWidth
    }

    private val animatorListenerAdapter: AnimatorListenerAdapter = object : AnimatorListenerAdapter() {
        override fun onAnimationStart(animation: Animator) {
            super.onAnimationStart(animation)
            if (!loading) background = originBackground
        }

        override fun onAnimationEnd(animation: Animator) {
            super.onAnimationEnd(animation)
            if (loading) background = morphBackground
            animator = null
        }

        override fun onAnimationCancel(animation: Animator?) {
            super.onAnimationCancel(animation)
            animator = null
        }
    }

    fun setLoading(loading: Boolean) {
        this.loading = loading

        animator?.run {
            if (isRunning)
                cancel()
        }
        if (loading) {
            progress.visibility = VISIBLE
            button.visibility = INVISIBLE
            layoutParams.width = morphWidth
            layoutParams.height = morphHeight
            background = morphBackground
        }
        else {
            background = originBackground
            layoutParams.width = originWidth
            layoutParams.height = originHeight
            button.visibility = VISIBLE
            progress.visibility = INVISIBLE
        }
        requestLayout()
    }

    fun setAnimatedLoading(loading: Boolean) {
        this.loading = loading

        animator?.run {
            if (isRunning)
                cancel()
        }
        if (loading) {
            progress.visibility = VISIBLE
            button.visibility = INVISIBLE

            // loading starting animator
            animator = ValueAnimator.ofInt(originWidth, morphWidth).apply {
                duration = animateDuration
                addListener(animatorListenerAdapter)
                addUpdateListener { animation: ValueAnimator ->
                    layoutParams.width = animation.animatedValue as Int
                    requestLayout()
                }
                start()
            }
        } else {
            val buttonWrapWidth = getWrapWidth(button)

            // loading ending animator
            animator = ValueAnimator.ofInt(morphWidth, originWidth).apply {
                duration = animateDuration
                addListener(animatorListenerAdapter)
                addUpdateListener { animation: ValueAnimator ->
                    val animatedValue = animation.animatedValue as Int
                    if (button.visibility != VISIBLE && buttonWrapWidth <= animatedValue) {
                        button.visibility = VISIBLE
                        progress.visibility = INVISIBLE
                    }
                    layoutParams.width = animatedValue
                    requestLayout()
                }
                start()
            }
        }
    }
}