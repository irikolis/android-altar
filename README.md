## About

This project is the addition to the [Caravan](https://bitbucket.org/irikolis/android-caravan) project.
Designed for performing rituals at the game. The app is used by the master planner to perform the ritual in the temple. All the rituals performed are sent to the server and used to calculate the logic of the game.

## Technology ([screenshots](misc/image))

This project takes advantage of many popular libraries and tools of the Android ecosystem.

* Tech-stack
	* [Dagger 2](https://github.com/google/dagger) is arguably the most used Dependency Injection framework for Android.
    * [Retrofit 2](https://github.com/square/retrofit) is a type-safe HTTP client for Android and Java.
    * [Room](https://github.com/square/retrofit) provides an abstraction layer over SQLite.
    * [RxJava 2](https://github.com/ReactiveX/RxJava) provides a simple way of asynchronous programming and handling multiple events, errors and termination of the event stream.
    * [Jetpack](https://developer.android.com/jetpack)
        * [Lifecycle](https://developer.android.com/topic/libraries/architecture/lifecycle) perform an action when lifecycle state changes.
        * [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) notify views about data change.
        * [DataBinding](https://developer.android.com/topic/libraries/data-binding) provides a way to tie the UI with business logic.
        * [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) store and manage UI-related data in a lifecycle conscious way.
    * [Cicerone](https://github.com/terrakok/Cicerone) makes the navigation in an Android app easy.
    * [Timber](https://github.com/JakeWharton/timber) simplifies the process of Logging statements and automatically handle debug/release issue by itself.
    * [Firebase Crashlytics](https://firebase.google.com/docs/crashlytics) is a realtime crash reporter.
    * Tests
        * [Unit Tests](https://en.wikipedia.org/wiki/Unit_testing) ([JUnit](https://junit.org/junit4/))
        * [Mockito](https://github.com/mockito/mockito) is used to mock interfaces.
        * [Mockk](https://mockk.io/) is used to mock interfaces, which offers support for Kotlin language features and constructs. 

* Architecture
    * Clean Architecture
    * MVVM (presentation layer)
